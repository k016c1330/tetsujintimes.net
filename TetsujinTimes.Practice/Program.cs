﻿using Newtonsoft.Json;

using System;
using System.IO;
using System.Linq;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataModels;
using System.ComponentModel;

using static System.Console;
using TetsujinTimes.Net.Models;
using System.Data.Entity;

namespace TetsujinTimes.Practice
{
	class Program
	{
		static void Main(string[] args)
		{
			var dbContext = new ApplicationDbContext();
			foreach (var department in dbContext.Departments)
			{
				WriteLine(JsonConvert.SerializeObject(department));
			}

			return;


			var dbSetProperties =
				typeof(ApplicationDbContext)
				.GetProperties()
				.Where(p => p.PropertyType.IsGenericType)
				.Where(p => p.PropertyType.GetGenericTypeDefinition() == typeof(IDbSet<>));

			foreach (var dbSetProperty in dbSetProperties)
			{
				//var genericTypes = dbSetProperty.PropertyType.GenericTypeArguments;
				//var genericDbSetType = typeof(IDbSet<>).crea

				WriteLine("{0}", dbSetProperty.Name);

				var dbSet = dbSetProperty.GetValue(dbContext);
				WriteLine(dbSet.ToString());
			}

			
			ReadKey();
		}


		private static void ReadCsv()
		{
			//var filename = "./sample.csv";
			//var stream = new FileStream(filename, FileMode.Open);

			//Console.WriteLine("読み取り開始");
			//var results = CsvManager.ReadAsListAsync(stream).Result;
			//Console.WriteLine("読み取り終了");

			//Console.WriteLine(JsonConvert.SerializeObject(results, Formatting.Indented));

			//foreach (var result in results)
			//{
			//	foreach (var key in result.Keys)
			//	{
			//		Console.WriteLine("{0}|{1}", key, result[key]);
			//	}
			//	Console.WriteLine();
			//}
		}

		private static void GetProperties()
		{
			// プロパティ情報を取得
			var info = typeof(Student);
			Console.WriteLine("{0}", info.Name);

			var properties = info.GetProperties();
			foreach (var property in properties)
			{
				Console.WriteLine("- {0}", property.Name);
				if (Attribute.GetCustomAttribute(property, typeof(DisplayNameAttribute)) is DisplayNameAttribute displayNameAttribute)
				{
					Console.WriteLine("-- DisplayName:{0}", displayNameAttribute.DisplayName);
				}
				if (Attribute.GetCustomAttribute(property, typeof(JsonPropertyAttribute)) is JsonPropertyAttribute jsonPropertyAttribute)
				{
					Console.WriteLine("-- PropertyName:{0}", jsonPropertyAttribute.PropertyName);
				}
				Console.WriteLine();
			}
		}
	}
}
