namespace TetsujinTimes.Net.Migrations
{
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;
	using TetsujinTimes.Net.Models;
	using TetsujinTimes.Net.Models.DataModels;

	internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
		}

		protected override void Seed(TetsujinTimes.Net.Models.ApplicationDbContext context)
		{
			//  This method will be called after migrating to the latest version.

			//  You can use the DbSet<T>.AddOrUpdate() helper extension method 
			//  to avoid creating duplicate seed data. E.g.
			//
			//    context.People.AddOrUpdate(
			//      p => p.FullName,
			//      new Person { FullName = "Andrew Peters" },
			//      new Person { FullName = "Brice Lambson" },
			//      new Person { FullName = "Rowan Miller" }
			//    );
			//

			//if (!context.Departments.Any())
			//{
			//	context.Departments.Add(new Department
			//	{
			//		DepartmentCode = "CD",
			//		DepartmentName = "情報処理科",
			//	});
			//}
			//context.SaveChanges();

			//if (!context.Students.Any())
			//{
			//	context.Students.AddOrUpdate(
			//		new Student
			//		{
			//			StudentId = "K016C2222",
			//			DepartmentId = context.Departments.FirstOrDefault().Id,
			//			Grade = 2,
			//			ClassNum = 4,
			//			UserId = null,
			//		}
			//	);
			//}

			//// 初期データの登録
			//if (!context.Departments.Any())
			//{
			//	new List<Department>
			//	{
			//		new Department { DepartmentCode = "CD", DepartmentName = "情報処理科" },
			//		new Department { DepartmentCode = "IS", DepartmentName = "ITスペシャリスト科" },
			//	}
			//	.ForEach(e => context.Departments.Add(e));
			//	context.SaveChanges();
			//}
			//if (!context.Classrooms.Any())
			//{
			//	new List<Classroom>
			//	{
			//		new Classroom{ ClassroomName="30711" },
			//		new Classroom{ ClassroomName="30712" },
			//		new Classroom{ ClassroomName="30713" },
			//	}
			//	.ForEach(e => context.Classrooms.Add(e));
			//	context.SaveChanges();
			//}
			//if (!context.Periods.Any())
			//{
			//	new List<Period>
			//	{
			//		new Period { Number = 1, StartTime = TimeSpan.Parse("09:00"), EndTime = TimeSpan.Parse("09:45") },
			//		new Period { Number = 2, StartTime = TimeSpan.Parse("09:45"), EndTime = TimeSpan.Parse("10:30") },
			//		new Period { Number = 3, StartTime = TimeSpan.Parse("10:40"), EndTime = TimeSpan.Parse("11:25") },
			//		new Period { Number = 4, StartTime = TimeSpan.Parse("11:25"), EndTime = TimeSpan.Parse("12:10") },
			//		new Period { Number = 5, StartTime = TimeSpan.Parse("13:00"), EndTime = TimeSpan.Parse("13:45") },
			//		new Period { Number = 6, StartTime = TimeSpan.Parse("13:45"), EndTime = TimeSpan.Parse("14:30") },
			//		new Period { Number = 7, StartTime = TimeSpan.Parse("14:40"), EndTime = TimeSpan.Parse("15:25") },
			//		new Period { Number = 8, StartTime = TimeSpan.Parse("15:25"), EndTime = TimeSpan.Parse("16:10") },
			//	}
			//	.ForEach(e => context.Periods.Add(e));
			//	context.SaveChanges();
			//}

			//if (!context.Users.OfType<Student>().Any())
			//{
			//	new List<Student>
			//	{
			//		new Student
			//		{
			//			StudentId = "K016C0000",
			//			Name = "情報太郎",
			//			Email = "taro@it-neec.jp",
			//			DepartmentId = context.Departments.First().Id,
			//			Grade = 1,
			//			ClassNum = 1,
			//		},
			//		new Student
			//		{
			//			StudentId = "K016C0001",
			//			Name = "情報次郎",
			//			Email = "jiro@it-neec.jp",
			//			DepartmentId = context.Departments.First().Id,
			//			Grade = 1,
			//			ClassNum = 3,
			//		},
			//		new Student
			//		{
			//			StudentId = "K016C0002",
			//			Name = "情報花子",
			//			Email = "hanako@it-neec.jp",
			//			DepartmentId = context.Departments.First().Id,
			//			Grade = 1,
			//			ClassNum = 3,
			//		},
			//	}
			//	.ForEach(e => context.Users.Add(e));
			//	context.SaveChanges();
			//}

			//if (!context.Users.OfType<Teacher>().Any())
			//{
			//	new List<Teacher>
			//	{
			//		new Teacher
			//		{
			//			TeacherId = 1,
			//			Name = "教師一郎",
			//			Email = "Teacher01@it-neec.jp",
			//		},
			//		new Teacher
			//		{
			//			TeacherId = 2,
			//			Name = "教師次郎",
			//			Email = "Teacher02@it-neec.jp",
			//		},
			//	}
			//	.ForEach(e => context.Users.Add(e));
			//	context.SaveChanges();
			//}

			//if (!context.Lessons.Any())
			//{
			//	new List<Lesson>
			//	{
			//		new Lesson
			//		{
			//			LessonCode = "KGCD016CD1044",
			//			LessonName = "キャリアデザイン４",
			//			TeacherId = context.Users.OfType<Teacher>().OrderBy(e => e.Id).First().Id,
			//			ClassroomId = context.Classrooms.OrderBy(e => e.Id).First().Id,
			//			WeekDay = DayOfWeek.Monday,
			//			StartPeriodId = context.Periods.OrderBy(e => e.Id).Skip(0).First().Id,
			//			EndPeriodId = context.Periods.OrderBy(e => e.Id).Skip(1).First().Id,
			//			Season = TetsujinSeason.Previous,
			//			Description = "キャリアデザイン４の授業",
			//			FirstDate = DateTime.Now.AddDays(1),
			//			TotalCount = 12,
			//		},
			//		new Lesson
			//		{
			//			LessonCode = "KGCD016CD1045",
			//			LessonName = "卒業制作",
			//			TeacherId = context.Users.OfType<Teacher>().OrderBy(e => e.Id).Skip(1).First().Id,
			//			ClassroomId = context.Classrooms.OrderBy(e => e.Id).Skip(1).First().Id,
			//			WeekDay = DayOfWeek.Friday,
			//			StartPeriodId = context.Periods.OrderBy(e => e.Id).Skip(2).First().Id,
			//			EndPeriodId = context.Periods.OrderBy(e => e.Id).Skip(3).First().Id,
			//			Season = TetsujinSeason.Later,
			//			Description = "卒業制作の授業",
			//			FirstDate = DateTime.Now.AddDays(100),
			//			TotalCount = 12,
			//		},
			//	}
			//	.ForEach(e => context.Lessons.Add(e));
			//	context.SaveChanges();
			//}
		}
	}
}
