﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Attributes
{
	/// <summary>
	/// アプリケーション独自の認証処理を実装
	/// </summary>
	public class ApplicationAuthorizeAttribute : AuthorizeAttribute
	{
		protected override bool IsAuthorized(HttpActionContext actionContext)
		{
			// ログインしたユーザIDを取得
			var userId = actionContext.RequestContext.Principal.Identity.GetUserId();
			var manager = actionContext.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

			// ログインしたユーザが存在すれば認証成功
			var result = manager.FindById(userId) != null;
				//.IsInRole(userId, "Administrator");

			return result;

		}
	}

	/// <summary>
	/// 教師ユーザーのみ認証を許可
	/// </summary>
	public class TeacherOnlyAuthorizeAttribute : AuthorizeAttribute
	{
		protected override bool IsAuthorized(HttpActionContext actionContext)
		{
			var userId = actionContext.RequestContext.Principal.Identity.GetUserId();
			var manager = actionContext.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

			// ユーザが教師かどうか判定
			var result = manager.FindById(userId) is Teacher;

			return result;
		}
	}
}