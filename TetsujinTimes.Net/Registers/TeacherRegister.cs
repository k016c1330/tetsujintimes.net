﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using System.Web;
//using TetsujinTimes.Net.Models;
//using TetsujinTimes.Net.Models.DataModels;
//using TetsujinTimes.Net.Registers.Base;

//namespace TetsujinTimes.Net.Registers
//{
//	public class TeacherRegister : DataRegister<Teacher>
//	{
//		private ApplicationDbContext _dbContext;
//		private UserRegister _userRegister;

//		public TeacherRegister(ApplicationDbContext dbContext, ApplicationUserManager userManager)
//		{
//			_dbContext = dbContext;
//			_userRegister = new UserRegister(userManager);
//		}
		
//		public async Task<Teacher> CreateAsync(CreateTeacherBindingModel model)
//		{
//			var user = await _userRegister.Create(model);
//			var teacher = new Teacher
//			{
//				TeacherId = model.TeacherId,
//				UserId = user.Id,
//			};
//			_dbContext.Teachers.Add(teacher);
//			await _dbContext.SaveChangesAsync();

//			return teacher;
//		}
//	}
//}