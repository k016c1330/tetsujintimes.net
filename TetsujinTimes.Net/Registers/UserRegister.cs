﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Registers.Base;

namespace TetsujinTimes.Net.Registers
{
	public class UserRegister : DataRegister<ApplicationUser>
	{
		public ApplicationUserManager _userManager;

		public UserRegister(ApplicationUserManager userManager)
		{
			_userManager = userManager;
		}


		public async Task<ApplicationUser> Create(RegisterBindingModel model)
		{
			var user = new ApplicationUser
			{
				UserName = model.UserName,
				Email = model.Email,
			};

			// ユーザの登録
			var userResult = await _userManager.CreateAsync(user);
			if (!userResult.Succeeded)
			{
				throw new ArgumentException("ユーザの登録に失敗しました");
			}

			// ロールの登録
			var roleResult = await _userManager.AddToRoleAsync(user.Id, model.Role);
			if (!roleResult.Succeeded)
			{
				await _userManager.DeleteAsync(user);
				throw new ArgumentException("ロールの登録に失敗しました");
			}

			return user;
		}

		/// <summary>
		/// 選択したロールを設定
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="role"></param>
		/// <returns></returns>
		public async Task<bool> SetRoleAsync(string userId, string role)
		{
			// ロールが見つからなければ失敗
			if (!ApplicationRole.DefaultRoles.Any(r => r.Name == role))
			{
				return false;
			}

			// 既にロールが設定されてれば成功
			if (await _userManager.IsInRoleAsync(userId, role))
			{
				return true;
			}

			// 既に登録されているロールを全削除
			var roles = await _userManager.GetRolesAsync(userId);
			foreach (var currentRole in roles)
			{
				await _userManager.RemoveFromRoleAsync(userId, currentRole);
			}

			// ロールを登録
			await _userManager.AddToRoleAsync(userId, role);

			return true;
		}

		public async Task<bool> ChangePasswordAsync(ApplicationUser user, ChangePasswordBindingModel passwordModel)
		{
			// パスワードを検証
			if (!await _userManager.CheckPasswordAsync(user, passwordModel.OldPassword))
			{
				return false;
			}

			var result = await _userManager.ChangePasswordAsync(user.Id, passwordModel.OldPassword, passwordModel.NewPassword);
			return result.Succeeded;
		}

		public async Task<ApplicationUser> DeleteAsync(ApplicationUser user)
		{
			await _userManager.DeleteAsync(user);
			return user;
		}
	}
}