﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using System.Web;
//using TetsujinTimes.Net.Models;
//using TetsujinTimes.Net.Models.DataModels;
//using TetsujinTimes.Net.Registers.Base;

//namespace TetsujinTimes.Net.Registers
//{
//	public class StudentRegister : DataRegister<Student>
//	{
//		private ApplicationDbContext _dbContext;
//		private UserRegister _userRegister;

//		public StudentRegister(ApplicationDbContext dbContext, ApplicationUserManager userManager)
//		{
//			_dbContext = dbContext;
//			_userRegister = new UserRegister(userManager);
//		}

//		public async Task<Student> CreateAsync(CreateStudentBindingModel model)
//		{
//			var user = await _userRegister.Create(model);
//			var student = new Student
//			{
//				StudentId = model.StudentId,
//				DepartmentId = model.DepartmentId,
//				Grade = model.Grade,
//				ClassNum = model.ClassNum,
//				UserId = user.Id,
//			};

//			_dbContext.Students.Add(student);
//			await _dbContext.SaveChangesAsync();

//			return student;
//		}

//		public async Task<Student> UpdateAsync(Student student, CreateStudentBindingModel studentModel, ChangePasswordBindingModel passwordModel)
//		{

//			return student;
//		}
//	}
//}