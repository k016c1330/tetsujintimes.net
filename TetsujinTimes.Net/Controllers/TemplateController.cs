﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TetsujinTimes.Net.Controllers
{
    public class TemplateController : Controller
    {
		// 汎用ビュー
		public ActionResult Default(string viewName)
		{
			return View(viewName);
		}
	}
}