﻿using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Web
{
	public class TeachersController : Controller
	{
		// データ取得元のAPIコントローラー
		private Api.TeachersController _apiController = new Api.TeachersController();
		public Api.TeachersController GetApiController()
		{
			_apiController.UserManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
			return _apiController;
		}

		// GET: Teachers
		public ActionResult Index()
		{
			return View(GetApiController().GetTeachers());
		}

		// GET: Teachers/Template
		public async Task<ActionResult> Template()
		{
			return View();
		}


		// GET: Teachers/Details/5
		public async Task<ActionResult> Details(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			
			// APIコントローラーからデータを取得
			var result = await GetApiController().GetTeacherAsync(id);
			if (result is OkNegotiatedContentResult<Teacher> suceededResult)
			{
				var teacher = suceededResult.Content;
				return View(teacher);
			}
			return HttpNotFound();
		}

		// GET: Teachers/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: Teachers/Create
		// 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
		// 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create(CreateTeacherBindingModel model)
		{
			if (ModelState.IsValid)
			{
				// APIコントローラーからデータを取得
				var result = await GetApiController().PostTeacherAsync(model);
				if (result is CreatedAtRouteNegotiatedContentResult<Teacher>)
				{
					return RedirectToAction("Index");
				}
			}

			// 作成失敗
			return View(model);
		}

		// GET: Teachers/Edit/5
		public async Task<ActionResult> Edit(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			// APIコントローラーからデータを取得
			var result = await GetApiController().GetTeacherAsync(id);
			if (result is OkNegotiatedContentResult<Teacher> suceededResult)
			{
				var model = new UpdateTeacherBindingModel(suceededResult.Content);
				return View(model);
			}
			return HttpNotFound();
		}

		// POST: Teachers/Edit/5
		// 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
		// 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit(string id, UpdateTeacherBindingModel model)
		{
			if (ModelState.IsValid)
			{
				// APIコントローラーからデータを取得
				var result = await GetApiController().PutTeacherAsync(id, model);
				if (result is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == HttpStatusCode.NoContent)
				{
					return RedirectToAction("Index");
				}
			}
			return View(model);
		}

		// GET: Teachers/Delete/5
		public async Task<ActionResult> Delete(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			var result = await GetApiController().GetTeacherAsync(id);
			if (result is OkNegotiatedContentResult<Teacher> suceededResult)
			{
				var teacher = suceededResult.Content;
				return View(teacher);
			}
			return HttpNotFound();
		}

		// POST: Teachers/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> DeleteConfirmed(string id)
		{
			await GetApiController().DeleteTeacherAsync(id);
			return RedirectToAction("Index");
		}
	}
}
