﻿using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Web
{
    public class StudentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

		// データ取得元のAPIコントローラー
		private Api.StudentsController _apiController = new Api.StudentsController();
		public Api.StudentsController GetApiController()
		{
			_apiController.UserManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
			return _apiController;
		}

		// GET: Students
		public ActionResult Index()
		{
			return View(GetApiController().GetStudents());
        }

		// GET: Students/Template
		public async Task<ActionResult> Template()
		{
			return View();
		}

		// GET: Students/Details/5
		public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

			// APIコントローラーからデータを取得
			var result = await GetApiController().GetStudentAsync(id);
			Student student = null;
			if (result is OkNegotiatedContentResult<Student> suceededResult)
			{
				student = suceededResult.Content;
			}
			else
			{
				return HttpNotFound();
			}
			return View(student);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName");
            return View();
		}

        // POST: Students/Create
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateStudentBindingModel model)
        {
            if (ModelState.IsValid)
            {
				// APIコントローラーからデータを取得
				var result = await GetApiController().PostStudentAsync(model);
				if (result is CreatedAtRouteNegotiatedContentResult<Student>)
				{
					return RedirectToAction("Index");
				}
			}

			ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", model.DepartmentId);
            return View(model);
        }

        // GET: Students/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

			// APIコントローラーからデータを取得
			var result = await GetApiController().GetStudentAsync(id);
			if (result is OkNegotiatedContentResult<Student> suceededResult)
			{
				var model = new UpdateStudentBindingModel(suceededResult.Content);
				ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", model.DepartmentId);
				return View(model);
			}

			return HttpNotFound();
		}

        // POST: Students/Edit/5
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string id, UpdateStudentBindingModel model)
		{
			if (ModelState.IsValid)
			{
				// APIコントローラーからデータを取得
				var result = await GetApiController().PutStudentAsync(id, model);
				if (result is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == HttpStatusCode.NoContent)
				{
					return RedirectToAction("Index");
				}
			}
			
			ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", model.DepartmentId);
			return View(model);
        }

        // GET: Students/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			
			var result = await GetApiController().GetStudentAsync(id);
			if (result is OkNegotiatedContentResult<Student> suceededResult)
			{
				var student = suceededResult.Content;
				ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", student.DepartmentId);
				return View(student);
			}

			return HttpNotFound();
		}

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
		{
			await GetApiController().DeleteStudentAsync(id);
			return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
