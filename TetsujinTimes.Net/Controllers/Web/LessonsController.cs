﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Web
{
    public class LessonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Lessons
        public ActionResult Index()
        {
            var lessons = db.Lessons.Include(l => l.ClassRoom).Include(l => l.EndPeriod).Include(l => l.StartPeriod).Include(l => l.Teacher);
            return View(lessons.ToList());
        }

		// GET: ___/Template
		public ActionResult Template()
		{
			return View();
		}

		// GET: Lessons/Details/5
		public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson lesson = db.Lessons.Find(id);
            if (lesson == null)
            {
                return HttpNotFound();
            }
            return View(lesson);
        }

        // GET: Lessons/Create
        public ActionResult Create()
		{
			var lesson = new Lesson();
			SetViewBag(lesson.TeacherId, lesson.ClassRoomId, lesson.StartPeriodId, lesson.EndPeriodId);
			return View(lesson);
		}

        // POST: Lessons/Create
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,LessonCode,LessonName,TeacherId,ClassRoomId,WeekDay,StartPeriodId,EndPeriodId,Description")] Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                db.Lessons.Add(lesson);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

			SetViewBag(lesson.TeacherId, lesson.ClassRoomId, lesson.StartPeriodId, lesson.EndPeriodId);
			return View(lesson);
        }

        // GET: Lessons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson lesson = db.Lessons.Find(id);
            if (lesson == null)
            {
                return HttpNotFound();
			}

			SetViewBag(lesson.TeacherId, lesson.ClassRoomId, lesson.StartPeriodId, lesson.EndPeriodId);
			return View(lesson);
        }

        // POST: Lessons/Edit/5
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LessonCode,LessonName,TeacherId,ClassRoomId,WeekDay,StartPeriodId,EndPeriodId,Description")] Lesson lesson)
		{
			if (ModelState.IsValid)
			{
				db.Entry(lesson).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			SetViewBag(lesson.TeacherId, lesson.ClassRoomId, lesson.StartPeriodId, lesson.EndPeriodId);
			return View(lesson);
		}

		// GET: Lessons/Delete/5
		public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson lesson = db.Lessons.Find(id);
            if (lesson == null)
            {
                return HttpNotFound();
            }
            return View(lesson);
        }

        // POST: Lessons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Lesson lesson = db.Lessons.Find(id);
            db.Lessons.Remove(lesson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

		private void SetViewBag(string teacherId = null, int classRoomId = 0, int startPeriodId = 0, int endPeriodId = 0)
		{
			ViewBag.TeacherId = new SelectList(db.Users.OfType<Teacher>(), nameof(Teacher.Id), nameof(Teacher.Name), teacherId);
			ViewBag.ClassRoomId = new SelectList(db.ClassRooms, nameof(ClassRoom.Id), nameof(ClassRoom.ClassRoomName), classRoomId);
			ViewBag.StartPeriodId = new SelectList(db.Periods, nameof(Period.Id), nameof(Period.Number), startPeriodId);
			ViewBag.EndPeriodId = new SelectList(db.Periods, nameof(Period.Id), nameof(Period.Number), endPeriodId);
		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
