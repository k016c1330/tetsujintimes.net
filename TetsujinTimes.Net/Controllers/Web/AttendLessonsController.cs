﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Web
{
    public class AttendLessonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AttendLessons
        public ActionResult Index()
        {
            var attendLessons = db.AttendLessons.Include(e => e.Lesson).Include(e => e.Student);
            return View(attendLessons.ToList());
        }

		// GET: ___/Template
		public ActionResult Template()
		{
			return View();
		}

		// GET: AttendLessons/Details/5
		public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttendLesson attendLesson = db.AttendLessons.Find(id);
            if (attendLesson == null)
            {
                return HttpNotFound();
            }
            return View(attendLesson);
        }

        // GET: AttendLessons/Create
        public ActionResult Create()
		{
			SetViewBag();
			return View();
        }

        // POST: AttendLessons/Create
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,LessonId,StudentId")] AttendLesson attendLesson)
		{
			if (ModelState.IsValid)
			{
				db.AttendLessons.Add(attendLesson);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			SetViewBag(attendLesson.LessonId, attendLesson.StudentId);
			return View(attendLesson);
		}
		
		// GET: AttendLessons/Edit/5
		public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttendLesson attendLesson = db.AttendLessons.Find(id);
            if (attendLesson == null)
            {
                return HttpNotFound();
            }

			SetViewBag(attendLesson.LessonId, attendLesson.StudentId);
			return View(attendLesson);
        }

        // POST: AttendLessons/Edit/5
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LessonId,StudentId")] AttendLesson attendLesson)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attendLesson).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
			}

			SetViewBag(attendLesson.LessonId, attendLesson.StudentId);
			return View(attendLesson);
        }

        // GET: AttendLessons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttendLesson attendLesson = db.AttendLessons.Find(id);
            if (attendLesson == null)
            {
                return HttpNotFound();
            }
            return View(attendLesson);
        }

        // POST: AttendLessons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AttendLesson attendLesson = db.AttendLessons.Find(id);
            db.AttendLessons.Remove(attendLesson);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

		private void SetViewBag(int lessonId = 0, string studentId = null)
		{
			ViewBag.LessonId = new SelectList(db.Lessons, nameof(Lesson.Id), nameof(Lesson.LessonName), lessonId);
			ViewBag.StudentId = new SelectList(db.Users.OfType<Student>(), nameof(Student.Id), nameof(Student.Name), studentId);
		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
