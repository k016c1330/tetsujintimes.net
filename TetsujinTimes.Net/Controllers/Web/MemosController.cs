﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Web
{
    public class MemosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Memos
        public ActionResult Index()
        {
            var memos = db.Memos.Include(m => m.Lesson).Include(m => m.Student);
            return View(memos.ToList());
        }

		// GET: ___/Template
		public ActionResult Template()
		{
			return View();
		}

		// GET: Memos/Details/5
		public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Memo memo = db.Memos.Find(id);
            if (memo == null)
            {
                return HttpNotFound();
            }
            return View(memo);
        }

        // GET: Memos/Create
        public ActionResult Create()
		{
			SetViewBag();
			return View();
        }

        // POST: Memos/Create
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,LessonId,StudentId,MemoAt,Content")] Memo memo)
		{
			if (ModelState.IsValid)
			{
				db.Memos.Add(memo);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			SetViewBag(memo.LessonId, memo.StudentId);
			return View(memo);
		}

		// GET: Memos/Edit/5
		public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Memo memo = db.Memos.Find(id);
            if (memo == null)
            {
                return HttpNotFound();
			}
			SetViewBag(memo.LessonId, memo.StudentId);
			return View(memo);
        }

        // POST: Memos/Edit/5
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LessonId,StudentId,MemoAt,Content")] Memo memo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(memo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
			}
			SetViewBag(memo.LessonId, memo.StudentId);
			return View(memo);
        }

        // GET: Memos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Memo memo = db.Memos.Find(id);
            if (memo == null)
            {
                return HttpNotFound();
            }
            return View(memo);
        }

        // POST: Memos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Memo memo = db.Memos.Find(id);
            db.Memos.Remove(memo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

		private void SetViewBag(int lessonId = 0, string studentId = null)
		{
			ViewBag.LessonId = new SelectList(db.Lessons, nameof(Lesson.Id), nameof(Lesson.LessonName), lessonId);
			ViewBag.StudentId = new SelectList(db.Users.OfType<Student>(), nameof(Student.Id), nameof(Student.Name), studentId);
		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
