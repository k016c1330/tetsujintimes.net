﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Web
{
    public class LessonDetailsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: LessonDetails
        public async Task<ActionResult> Index()
        {
            var lessonDetails = db.LessonDetails.Include(l => l.Lesson);
            return View(await lessonDetails.ToListAsync());
        }

		// GET: ___/Template
		public ActionResult Template()
		{
			return View();
		}

		// GET: LessonDetails/Details/5
		public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LessonDetail lessonDetail = await db.LessonDetails.FindAsync(id);
            if (lessonDetail == null)
            {
                return HttpNotFound();
            }
            return View(lessonDetail);
        }

        // GET: LessonDetails/Create
        public ActionResult Create()
        {
            ViewBag.LessonId = new SelectList(db.Lessons, "Id", "LessonCode");
            return View();
        }

        // POST: LessonDetails/Create
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,LessonId,Date,StartTime,EndTime,Description")] LessonDetail lessonDetail)
        {
            if (ModelState.IsValid)
            {
                db.LessonDetails.Add(lessonDetail);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LessonId = new SelectList(db.Lessons, "Id", "LessonCode", lessonDetail.LessonId);
            return View(lessonDetail);
        }

        // GET: LessonDetails/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LessonDetail lessonDetail = await db.LessonDetails.FindAsync(id);
            if (lessonDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.LessonId = new SelectList(db.Lessons, "Id", "LessonCode", lessonDetail.LessonId);
            return View(lessonDetail);
        }

        // POST: LessonDetails/Edit/5
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,LessonId,Date,StartTime,EndTime,Description")] LessonDetail lessonDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lessonDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LessonId = new SelectList(db.Lessons, "Id", "LessonCode", lessonDetail.LessonId);
            return View(lessonDetail);
        }

        // GET: LessonDetails/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LessonDetail lessonDetail = await db.LessonDetails.FindAsync(id);
            if (lessonDetail == null)
            {
                return HttpNotFound();
            }
            return View(lessonDetail);
        }

        // POST: LessonDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            LessonDetail lessonDetail = await db.LessonDetails.FindAsync(id);
            db.LessonDetails.Remove(lessonDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
