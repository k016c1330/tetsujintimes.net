﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Web
{
    public class AttendancesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Attendances
        public async Task<ActionResult> Index()
        {
            var attendances = db.Attendances.Include(a => a.AttendLesson).Include(a => a.LessonDetail);
            return View(await attendances.ToListAsync());
        }

		// GET: ___/Template
		public async Task<ActionResult> Template()
		{
			return View();
		}

		// GET: Attendances/Details/5
		public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = await db.Attendances.FindAsync(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            return View(attendance);
        }

        // GET: Attendances/Create
        public ActionResult Create()
        {
            ViewBag.AttendLessonId = new SelectList(db.AttendLessons, "Id", "StudentId");
            ViewBag.LessonDetailId = new SelectList(db.LessonDetails, "Id", "Description");
            return View();
        }

        // POST: Attendances/Create
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,LessonDetailId,AttendLessonId,AttendanceTime,Attended,WasLate,LeftEarly")] Attendance attendance)
        {
            if (ModelState.IsValid)
            {
                db.Attendances.Add(attendance);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AttendLessonId = new SelectList(db.AttendLessons, "Id", "StudentId", attendance.AttendLessonId);
            ViewBag.LessonDetailId = new SelectList(db.LessonDetails, "Id", "Description", attendance.LessonDetailId);
            return View(attendance);
        }

        // GET: Attendances/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = await db.Attendances.FindAsync(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            ViewBag.AttendLessonId = new SelectList(db.AttendLessons, "Id", "StudentId", attendance.AttendLessonId);
            ViewBag.LessonDetailId = new SelectList(db.LessonDetails, "Id", "Description", attendance.LessonDetailId);
            return View(attendance);
        }

        // POST: Attendances/Edit/5
        // 過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        // 詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 を参照してください。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,LessonDetailId,AttendLessonId,AttendanceTime,Attended,WasLate,LeftEarly")] Attendance attendance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attendance).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AttendLessonId = new SelectList(db.AttendLessons, "Id", "StudentId", attendance.AttendLessonId);
            ViewBag.LessonDetailId = new SelectList(db.LessonDetails, "Id", "Description", attendance.LessonDetailId);
            return View(attendance);
        }

        // GET: Attendances/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendance attendance = await db.Attendances.FindAsync(id);
            if (attendance == null)
            {
                return HttpNotFound();
            }
            return View(attendance);
        }

        // POST: Attendances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Attendance attendance = await db.Attendances.FindAsync(id);
            db.Attendances.Remove(attendance);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
