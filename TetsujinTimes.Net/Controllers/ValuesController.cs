﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using TetsujinTimes.Net.Attributes;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.Excel;

namespace TetsujinTimes.Net.Controllers
{
	public class ValuesController : ApiController
	{
		// GET api/values
		public string Get()
		{
			//return new string[] { "value1", "value2" };

			// 認証されたユーザのIDを返す
			var userId = User.Identity.GetUserId();

			if (userId == null)
			{
				return "ログインしていません。";
			}

			// 指定されたIDのユーザを取得
			var manager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
			var user = manager.FindById(userId);

			var userName = user.Name;
			var role = string.Join(", ", manager.GetRoles(userId));

			return
				"[" + userId + "]" +
				userName + "/" +
				role
				;
		}

		// GET api/values/5
		public string Get(int id)
		{
			return "value";
		}

		// POST api/values
		public void Post([FromBody]string value)
		{
		}

		// PUT api/values/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
		}


		private async Task removeAllAsync<TEntity>(IDbSet<TEntity> dbSet) where TEntity : class
		{
			await dbSet.ForEachAsync(e => dbSet.Remove(e));
		}


		/// <summary>
		/// すべてのデータを削除します。
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("api/allclear")]
		public async Task<IHttpActionResult> AllClearAsync()
		{
			var db = new ApplicationDbContext();

			await removeAllAsync(db.Classrooms);
			await db.SaveChangesAsync();
			await removeAllAsync(db.Periods);
			await db.SaveChangesAsync();
			await removeAllAsync(db.Departments);
			await db.SaveChangesAsync();
			await removeAllAsync(db.Classrooms);
			await db.SaveChangesAsync();
			await removeAllAsync(db.Groups);
			await db.SaveChangesAsync();
			await removeAllAsync(db.Users);
			await db.SaveChangesAsync();

			// ログイン用の管理ユーザーを登録
			var adminUser = new Teacher
			{
				Name = "Administrator",
				Email = "Administrator@admin.jp",
				TeacherId = 0,
			};
			var manager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
			await manager.CreateAsync(adminUser);
			await manager.AddPasswordAsync(adminUser.Id, adminUser.Email);
			
			return Ok("すべてのデータを削除しました。");
		}

		// 読み書きに利用するリストの定義
		private List<IUploadableManager> Managers = new List<IUploadableManager>()
			{
				new ClassroomsManager(),
				new PeriodsManager(),
				new DepartmentsManager(),
				new TeachersManager(),
				new StudentsManager(),
				new LessonsManager(),
				new GroupsManager(),
				new AttendGroupsManager(),
				new LessonGroupsManager(),
				new MemosManager(),
				//new TimeTablesManager(),
				//new AttendLessonsManager(),
			};

		/// <summary>
		/// 全てのデータをExcelファイルとして取得
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("api/getxlsx")]
		public async Task<IHttpActionResult> GetXlsx()
		{
			// 初期化
			foreach (var manager in Managers)
			{
				manager.OwinContext = Request.GetOwinContext();
				manager.User = User;
			}
			// 処理
			var workbook = WorkbookManager.CreateWorkbook();
			foreach (var manager in Managers.OfType<IWritableToWorkbook>())
			{
				manager.WriteToWorkbook(workbook);
			}
			var stream = workbook.GetStream();

			HttpResponseMessage response = new HttpResponseMessage
			{
				Content = new StreamContent(stream),
			};
			response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = $"TetsujinTimes.xlsx",
			};

			return ResponseMessage(response);
		}

		/// <summary>
		/// Excelファイルからデータを読み込み
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		[Route("api/getxlsx")]
		public async Task<IHttpActionResult> UploadXlsx()
		{
			if (!Request.Content.IsMimeMultipartContent())
			{
				return BadRequest();
			}

			var provider = await Request.Content.ReadAsMultipartAsync();
			var stream = await provider.Contents.FirstOrDefault()?.ReadAsStreamAsync();
			if (stream.Length == 0) return BadRequest("ファイルが空です。");


			// 初期化
			foreach (var manager in Managers)
			{
				manager.OwinContext = Request.GetOwinContext();
				manager.User = User;
			}
			// 処理
			var results = new List<IManagerResult>();
			foreach (var manager in Managers)
			{
				var result = await manager.UploadAsync(stream);
				results.Add(result);
			}

			return Ok(results);
		}

	}
}
