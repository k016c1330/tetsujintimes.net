﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Title = "Home Page";
			return View();
		}

		// GET: Teachers/Template
		public async Task<ActionResult> Template()
		{
			return View();
		}

	}
}
