﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class SchedulesController : ApiController
	{
		private ApplicationDbContext db = new ApplicationDbContext();

		[HttpGet]
		[Route("api/schedules")]
		public IHttpActionResult GetSchedule()
		{
			if (!User.Identity.IsAuthenticated)
			{
				return BadRequest("認証されていません。");
			}
			string id = User.Identity.GetUserId();
			var user = db.Users.Find(id);
			if (user is Student student)
			{
				return GetSchedule(student.StudentId);
			}
			return BadRequest("生徒の学籍番号を指定してください。");
		}

		[HttpGet]
		[Route("api/schedules/{studentId}")]
		public IHttpActionResult GetSchedule(string studentId)
		{
			// 指定された学籍番号に該当する生徒情報を取得
			var student = db.Users.OfType<Student>().FirstOrDefault(e => e.StudentId == studentId);
			if (student == null)
			{
				return BadRequest("要求された学籍番号の生徒は存在しません。");
			}

			var attendGroups = db.AttendGroups.Where(e => e.StudentId == student.Id);
			var groups = db.Groups.Where(e => attendGroups.Any(ag => e.Id == ag.GroupId));
			var lessonGroups = db.LessonGroups.Where(e => groups.Any(g => e.GroupId == g.Id));
			var lessons = db.Lessons
				// AttendGroupsから該当する時間割情報を検索
				.Where(e => lessonGroups.Any(lg => e.Id == lg.LessonId))
				//.Where(e => db.TimeTables
				//	.Where(t => t.DepartmentId == student.DepartmentId)
				//	.Where(t => t.Grade == student.Grade)
				//	.Where(t => t.ClassNum == student.ClassNum)
				//	.Any())
				.ToList()
				.Select(e => new
				{
					lessonCode = e.LessonCode,
					lessonName = e.LessonName,
					teacherName = e.TeacherName,
					classroomName = e.ClassroomName,
					weekDayNumber = e.WeekDayNumber,
					weekDay = Enum.GetName(typeof(DayOfWeek), e.WeekDay),
					startPeriod = e.StartPeriodNumber,
					endPeriod = e.EndPeriodNumber,
					startTime = e.StartTime,
					endTime = e.EndTime,
					firstDate = e.FirstDate.ToShortDateString(),
					totalCount = e.TotalCount,
					description = e.Description,
					groups = groups.Select(g => new
					{
						groupCode = g.GroupCode,
						description = g.Description,
					}),
					details = Enumerable.Range(1, e.TotalCount / (e.EndPeriodNumber - e.StartPeriodNumber) ?? 1)
						.Select(i => new {
							count = i,
							date = e.FirstDate.AddDays((i - 1) * 7).ToShortDateString(),
							})
						.Select(t => new
						{
							//lessonCode = e.LessonCode,
							//lessonName = e.LessonName,
							//teacherName = e.TeacherName,
							//classroomName = e.ClassroomName,
							//weekDayNumber = e.WeekDayNumber,
							//weekDay = Enum.GetName(typeof(DayOfWeek), e.WeekDay),
							//startPeriod = e.StartPeriodNumber,
							//endPeriod = e.EndPeriodNumber,
							//startTime = e.StartTime,
							//endTime = e.EndTime,
							t.count,
							t.date,
						})
				});

			var result = new
			{
				student = new
				{
					name = student.Name,
					studentId = student.StudentId,
					email = student.Email,
					departmentCode = student.DepartmentCode,
					departmentName = student.DepartmentName,
					grade = student.Grade,
					classNum = student.ClassNum,
				},
				lessons,
				lessonCount = lessons.Count(),
			};
			return Ok(result);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
