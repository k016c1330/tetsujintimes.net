﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Api.Interfaces
{
	public interface IRestController<TKey, TValue>
	{
		Task<IHttpActionResult> DeleteAsync(TKey id);
		Task<IHttpActionResult> GetAsync();
		Task<IHttpActionResult> GetAsync(TKey id);
		Task<IHttpActionResult> PostAsync(TValue entity);
		Task<IHttpActionResult> PutAsync(TKey id, TValue entity);
	}
}