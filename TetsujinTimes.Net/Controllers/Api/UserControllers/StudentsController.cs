﻿using System.Threading.Tasks;
using System.Web.Http;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class StudentsController : AbstractUsersController<string, Student>
	{
		public StudentsController() : base(new StudentsManager())
		{
		}

		public StudentsController(AbstractUserManager<string, Student> userManager) : base(userManager)
		{
		}

		[HttpPost]
		[Route("upload/students")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}
	}
}