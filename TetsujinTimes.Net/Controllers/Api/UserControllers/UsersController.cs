﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class UsersController : AbstractUsersController<string, ApplicationUser>
	{
		public UsersController() : base(new UsersManager())
		{
		}

		public UsersController(AbstractUserManager<string, ApplicationUser> userManager) : base(userManager)
		{
		}
	}
}
