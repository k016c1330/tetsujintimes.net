﻿using System.Threading.Tasks;
using System.Web.Http;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class TeachersController : AbstractUsersController<int, Teacher>
	{
		public TeachersController() : base(new TeachersManager())
		{

		}

		public TeachersController(AbstractUserManager<int, Teacher> userManager) : base(userManager)
		{
		}

		[HttpPost]
		[Route("upload/teachers")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}
	}
}