﻿using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.NotificationHubs.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TetsujinTimes.Net.Attributes;
using static TetsujinTimes.Net.Controllers.Api.NotificationRegisterController;

namespace TetsujinTimes.Net.Models.DataModels
{
	[TeacherOnlyAuthorize]
    public class NotificationsController : ApiController
	{
		private NotificationHubClient Hub;

		public NotificationsController()
		{
			var connectionString = "Endpoint=sb://tetsujintimes.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=QkIgvaihSSncY1B6aRwoqVzQCeVTV1qdUA+NukYH1kk=";
			var notificationHubPath = "TetsujinTimesNotification";
			Hub = NotificationHubClient.CreateClientFromConnectionString(connectionString, notificationHubPath);
		}

		public IHttpActionResult Get()
		{
			return BadRequest("このAPIはGet Methodには対応していません。");
		}

		// POST api/notification/
		public async Task<IHttpActionResult> PostAsync([FromBody] Notification notification)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var user = HttpContext.Current.User.Identity.Name;
			var db = new ApplicationDbContext();
			var title = notification.Title;
			//var teacherId = notification.TeacherId;
			//var user = db.Users.OfType<Teacher>().FirstOrDefault(t => t.Id == teacherId)?.Name;
			var message = notification.Description;

			string[] userTag = new string[2];
			userTag[0] = "username:" + "Any";
			userTag[1] = "from:" + user;

			Microsoft.Azure.NotificationHubs.NotificationOutcome outcome = null;
			HttpStatusCode ret = HttpStatusCode.InternalServerError;


			var pns = "gcm";
			switch (pns.ToLower())
			{
				case "wns":
					// Windows 8.1 / Windows Phone 8.1
					var toast = @"<toast><visual><binding template=""ToastText01""><text id=""1"">" +
								"From " + user + ": " + message + "</text></binding></visual></toast>";
					outcome = await Notifications.Instance.Hub.SendWindowsNativeNotificationAsync(toast, userTag);
					break;
				case "apns":
					// iOS
					var alert = "{\"aps\":{\"alert\":\"" + "From " + user + ": " + message + "\"}}";
					outcome = await Notifications.Instance.Hub.SendAppleNativeNotificationAsync(alert, userTag);
					break;
				case "gcm":
					// Android
					var notif = "{ \"data\" : {\"message\":\"" + "件名：" + title + "\r\n送信者： " + user + "\r\n" + message + "\"}}";
					outcome = await Notifications.Instance.Hub.SendGcmNativeNotificationAsync(notif);//, userTag);
					break;
			}

			if (outcome != null)
			{
				if (!((outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Abandoned) ||
					(outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Unknown)))
				{
					ret = HttpStatusCode.OK;
				}
			}

			return Ok(notification);
		}
	}
}
