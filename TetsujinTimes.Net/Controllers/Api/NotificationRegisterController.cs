﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.NotificationHubs.Messaging;
using TetsujinTimes.Net.Models;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class NotificationRegisterController : ApiController
	{
		private NotificationHubClient hub;

		public NotificationRegisterController()
		{
			hub = Notifications.Instance.Hub;
		}

		public class DeviceRegistration
		{
			public string Platform { get; set; }
			public string Handle { get; set; }
			public string[] Tags { get; set; }
		}

		// POST api/NotificationRegister
		public async Task<string> Post(string handle = null)
		{
			string newRegistrationId = null;
			if (handle != null)
			{
				var registrations = await hub.GetRegistrationsByChannelAsync(handle, 100);
				foreach (var registration in registrations)
				{
					if (newRegistrationId == null)
					{
						newRegistrationId = registration.RegistrationId;
					}
					else
					{
						await hub.DeleteRegistrationAsync(newRegistrationId);
					}
				}
			}

			if (newRegistrationId == null)
			{
				newRegistrationId = await hub.CreateRegistrationIdAsync();
			}
			return newRegistrationId;
		}

		// PUT api/NotificationRegister/
		public async Task<HttpResponseMessage> Put(string id, DeviceRegistration deviceUpdate)
		{
			RegistrationDescription registration = null;
			switch (deviceUpdate.Platform)
			{
				case "apns:":
					registration = new AppleRegistrationDescription(deviceUpdate.Handle);
					break;
				case "gcm:":
					registration = new GcmRegistrationDescription(deviceUpdate.Handle);
					break;
				default:
					throw new HttpResponseException(HttpStatusCode.BadRequest);
			}
			registration.RegistrationId = id;
			var username = HttpContext.Current.User.Identity.Name;

			registration.Tags = new HashSet<string>(deviceUpdate.Tags);
			registration.Tags.Add("username:" + username);

			try
			{
				await hub.CreateOrUpdateRegistrationAsync(registration);
			}
			catch (MessagingException e)
			{
				ReturnGoneIfHubResponseIsGone(e);
			}

			return Request.CreateResponse(HttpStatusCode.OK);
		}
		// DELETE api/register/5
		public async Task<HttpResponseMessage> Delete(string id)
		{
			await hub.DeleteRegistrationAsync(id);
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		private void ReturnGoneIfHubResponseIsGone(MessagingException e)
		{
			var webex = e.InnerException as WebException;
			if (webex.Status == WebExceptionStatus.ProtocolError)
			{
				var response = (HttpWebResponse)webex.Response;
				if (response.StatusCode == HttpStatusCode.Gone)
					throw new HttpRequestException(HttpStatusCode.Gone.ToString());
			}
		}

	}
}
	