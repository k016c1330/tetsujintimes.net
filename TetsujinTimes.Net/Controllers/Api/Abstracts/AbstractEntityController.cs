﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Interfaces;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Controllers.Api.Abstracts
{
	/// <summary>
	/// EntityManagerへ処理を委譲してREST機能を提供するAPIコントローラーの抽象クラス
	/// </summary>
	/// <typeparam name="TValue"></typeparam>
	public abstract class AbstractEntityController<TKey, TValue> :
		ApiController, IRestController<TKey, TValue>
		where TValue : AbstractEntity
	{
		protected AbstractEntityManager<TKey, TValue> Manager { get; set; }

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="manager">処理を委譲するエンティティマネージャ</param>
		public AbstractEntityController(AbstractEntityManager<TKey, TValue> manager)
		{
			this.Manager = manager;
		}

		/// <summary>
		/// エンティティマネージャを初期化して取得します。
		/// </summary>
		/// <returns></returns>
		protected virtual AbstractEntityManager<TKey, TValue> InitializeManager()
		{
			this.Manager.OwinContext = Request.GetOwinContext();
			this.Manager.User = this.User;
			return this.Manager;
		}

		// GET: api/{controller}
		public virtual async Task<IHttpActionResult> GetAsync()
		{
			var result = await InitializeManager().QueryAsync(GetQueryDictionary());

			if (result is HttpResponseMessageResult responseResult)
			{
				return ResponseMessage(responseResult.Response);
			}
			else if (result is QuerySucceededManagerResult<IQueryable<TValue>> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else
			{
				return BadRequest();
			}
		}

		// GET: api/{controller}/{id}
		public virtual async Task<IHttpActionResult> GetAsync(TKey id)
		{
			var result = await InitializeManager().GetAsync(id);
			if (result is GetSucceededManagerResult<TValue> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// POST: api/{controller}/
		public virtual async Task<IHttpActionResult> PostAsync(TValue entity)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var result = await InitializeManager().CreateAsync(entity);
			if (result is CreateSucceededManagerResult<TValue> suceeded)
			{
				return CreatedAtRoute("DefaultApi", new { id = entity.Identity }, entity);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// PUT: api/{controller}/{id}
		public virtual async Task<IHttpActionResult> PutAsync(TKey id, TValue entity)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var result = await InitializeManager().UpdateAsync(id, entity);
			if (result is UpdateSucceededManagerResult<TValue> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// DELETE: api/{controller}/{id}
		public virtual async Task<IHttpActionResult> DeleteAsync(TKey id)
		{
			var result = await InitializeManager().DeleteAsync(id);
			if (result is DeleteSucceededManagerResult<TValue> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// POST: upload/{controller}
		// protectedで実装して派生クラスでpublicに
		protected virtual async Task<IHttpActionResult> UploadAsync()
		{
			if (!Request.Content.IsMimeMultipartContent())
			{
				return BadRequest();
			}

			var provider = await Request.Content.ReadAsMultipartAsync();
			var stream = await provider.Contents.FirstOrDefault()?.ReadAsStreamAsync();
			if (stream.Length == 0) return BadRequest("ファイルが空です。");

			var manager = InitializeManager();
			var result = await manager.UploadAsync(stream);

			return Ok(result);
		}

		/// <summary>
		/// リクエストのクエリ文字列を辞書形式で取得
		/// </summary>
		/// <returns></returns>
		protected IDictionary<string, string> GetQueryDictionary()
		{
			return Request.GetQueryNameValuePairs().ToDictionary(e => e.Key, e => e.Value);
		}
	}
}
