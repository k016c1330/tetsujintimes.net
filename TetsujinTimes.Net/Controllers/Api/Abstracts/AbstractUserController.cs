﻿using ClosedXML.Excel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web.Http;
using TetsujinTimes.Net.Controllers.Api.Interfaces;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Controllers.Api.Abstracts
{
	public abstract class AbstractUsersController<TKey, TValue> :
		ApiController, IRestController<TKey, TValue>
		where TValue : ApplicationUser
	{
		public AbstractUserManager<TKey, TValue> Manager { get; set; }

		public AbstractUsersController(AbstractUserManager<TKey, TValue> manager)
		{
			this.Manager = manager;
		}

		protected virtual AbstractUserManager<TKey, TValue> InitializeManager()
		{
			this.Manager.OwinContext = Request.GetOwinContext();
			this.Manager.User = this.User;
			return this.Manager;
		}

		// GET: api/{controller}
		public virtual async Task<IHttpActionResult> GetAsync()
		{
			var result = await InitializeManager().QueryAsync(GetQueryDictionary());

			if (result is HttpResponseMessageResult responseResult)
			{
				return ResponseMessage(responseResult.Response);
			}
			else if (result is QuerySucceededManagerResult<IQueryable<TValue>> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else
			{
				return BadRequest();
			}
		}

		// GET: api/{controller}/{Id}
		public virtual async Task<IHttpActionResult> GetAsync(TKey id)
		{
			var result = await InitializeManager().GetAsync(id);
			if (result is GetSucceededManagerResult<TValue> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// POST: api/{controller}
		public virtual async Task<IHttpActionResult> PostAsync(TValue entity)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var result = await InitializeManager().CreateAsync(entity);
			if (result is CreateSucceededManagerResult<TValue> suceeded)
			{
				return CreatedAtRoute("DefaultApi", new { id = entity.Identity }, entity);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// PUT: api/{controller}/{id}
		public virtual async Task<IHttpActionResult> PutAsync(TKey id, TValue entity)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var result = await InitializeManager().UpdateAsync(id, entity);
			if (result is UpdateSucceededManagerResult<TValue> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// DELETE: api/{controller}/{id}
		public virtual async Task<IHttpActionResult> DeleteAsync(TKey id)
		{
			var result = await InitializeManager().DeleteAsync(id);
			if (result is DeleteSucceededManagerResult<TValue> suceeded)
			{
				return Ok(suceeded.Result);
			}
			else if (result is NotFoundMangerResult)
			{
				return NotFound();
			}
			else if (result is FailedManagerResult failed)
			{
				return BadRequest(failed.Message);
			}
			else
			{
				return BadRequest();
			}
		}

		// POST: upload/{controller}
		// protectedで実装して派生クラスでpublicに
		protected virtual async Task<IHttpActionResult> UploadAsync()
		{
			if (!Request.Content.IsMimeMultipartContent())
			{
				return BadRequest();
			}

			var provider = await Request.Content.ReadAsMultipartAsync();
			var stream = await provider.Contents.FirstOrDefault()?.ReadAsStreamAsync();
			if (stream.Length == 0) return BadRequest("ファイルが空です。");
			
			var manager = InitializeManager();
			var result = await manager.UploadAsync(stream);
			
			return Ok(result);
		}

		/// <summary>
		/// リクエストのクエリ文字列を辞書形式で取得
		/// </summary>
		/// <returns></returns>
		protected IDictionary<string, string> GetQueryDictionary()
		{
			return Request.GetQueryNameValuePairs().ToDictionary(e => e.Key, e => e.Value);
		}
	}
}
