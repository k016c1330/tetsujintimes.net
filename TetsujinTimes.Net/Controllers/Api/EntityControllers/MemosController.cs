﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;

using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class MemosController :
		AbstractEntityController<string, Memo>
	{
		public MemosController() : base(new MemosManager()) { }

		public MemosController(AbstractEntityManager<string, Memo> entityManager) : base(entityManager)
		{
		}


		private ApplicationDbContext DbContext => Request.GetOwinContext().Get<ApplicationDbContext>();

		public override async Task<IHttpActionResult> PostAsync(Memo entity)
		{
			// 作成時刻を現在時刻に設定
			entity.MemoAt = DateTime.Now;

			return await base.PostAsync(entity);
		}

		
		[HttpPost]
		[Route("upload/memos")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}


		// 追加メソッド

		// GET: api/Memos/{lessonCode}
		[Route("api/Memos/Memo/{lessonCode}")]
		[ResponseType(typeof(Memo))]
		public IHttpActionResult GetMemo(string lessonCode)
		{
			if (!User.Identity.IsAuthenticated)
			{
				return BadRequest("認証されていません。");
			}
			string id = User.Identity.GetUserId();
			var user = DbContext.Users.Find(id);
			if (user is Student student)
			{
				return GetMemo(lessonCode, student.StudentId);
			}
			return BadRequest("生徒の学籍番号を指定してください。");
		}

		// GET: api/Memos/Get/{lessonCode}/{studentId}
		[Route("api/Memos/Memo/{lessonCode}/{studentId}")]
		[ResponseType(typeof(Memo))]
		public IHttpActionResult GetMemo(string lessonCode, string studentId)
		{
			// 授業情報を取得
			var lesson = DbContext.Lessons.FirstOrDefault(e => e.LessonCode.Equals(lessonCode, StringComparison.OrdinalIgnoreCase));
			if (lesson == null)
			{
				return BadRequest("授業コードが見つかりませんでした。");
			}

			// 生徒情報を取得
			var student = DbContext.Users.OfType<Student>().FirstOrDefault(e => e.StudentId == studentId);
			if (student == null)
			{
				return BadRequest("学籍番号が見つかりませんでした。");
			}

			var memo = DbContext.Memos.FirstOrDefault(e => e.LessonId == lesson.Id && e.StudentId == student.Id);
			if (memo == null)
			{
				return NotFound();
			}

			return Ok(memo);
		}
	}
}
