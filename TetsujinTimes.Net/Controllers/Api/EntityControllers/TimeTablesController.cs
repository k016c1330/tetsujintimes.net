﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using System.Threading.Tasks;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class TimeTablesController :
		AbstractEntityController<string, TimeTable>
	{
		public TimeTablesController() : base(new TimeTablesManager()) { }

		public TimeTablesController(AbstractEntityManager<string, TimeTable> entityManager) : base(entityManager)
		{
		}

		[HttpPost]
		[Route("upload/timetables")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}

		private ApplicationDbContext DbContext => Request.GetOwinContext().Get<ApplicationDbContext>();

		// GET: api/TimeTables/{departmentCode}/{grade}/{classNum}
		// モバイル側からのデータを一括して取得
		[HttpGet]
		[Route("api/TimeTables/{departmentCode}/{grade}/{classNum}")]
		public IQueryable<object> GetTimeTables(string departmentCode, int grade, int classNum)
		{
			// 変換するデータを指定
			var query = DbContext.TimeTables
				.Include(e => e.Department)
				.Where(e =>
					e.Department.DepartmentCode == departmentCode &&
					e.Grade == grade &&
					e.ClassNum == classNum
				)
				.Include(e => e.Lesson)
				.Include(e => e.Lesson.Teacher)
				.Include(e => e.Lesson.Classroom)
				.Include(e => e.Lesson.StartPeriod)
				.Include(e => e.Lesson.EndPeriod)
				.Select(e => new
				{
					TimeTableId = e.Id,
					LessonCode = e.Lesson.LessonCode,
					LessonName = e.Lesson.LessonName,
					WeekDay = e.Lesson.WeekDay + 1,
					StartTime = e.Lesson.StartPeriod.StartTime,
					EndTime = e.Lesson.EndPeriod.EndTime,
					Description = e.Lesson.Description,
					ClassRoomName = e.Lesson.Classroom.ClassroomName,
					TeacherId = e.Lesson.Teacher.TeacherId,
					TeacherName = e.Lesson.Teacher.Name,
				});

			return query;
		}
	}
}