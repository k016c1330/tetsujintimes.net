﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using System.Threading.Tasks;

namespace TetsujinTimes.Net.Controllers.Api
{
    public class LessonsController :
		AbstractEntityController<string, Lesson>
	{
		public LessonsController() : base(new LessonsManager()) { }

		public LessonsController(AbstractEntityManager<string, Lesson> entityManager) : base(entityManager)
		{
		}

		[HttpPost]
		[Route("upload/lessons")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}
	}
}