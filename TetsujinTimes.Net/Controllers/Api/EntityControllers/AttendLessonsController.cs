﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using System.Threading.Tasks;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class AttendLessonsController :
		AbstractEntityController<string, AttendLesson>
	{
		public AttendLessonsController(AbstractEntityManager<string, AttendLesson> entityManager) : base(entityManager)
		{
		}

		public AttendLessonsController() :
			base(new AttendLessonsManager())
		{
		}

		[HttpPost]
		[Route("upload/attendlessons")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}
	}
}