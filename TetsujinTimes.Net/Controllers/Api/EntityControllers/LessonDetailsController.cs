﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;

namespace TetsujinTimes.Net.Controllers.Api
{
	public class LessonDetailsController :
		AbstractEntityController<string, LessonDetail>
	{
		public LessonDetailsController() : base(new LessonDetailsManager()) { }

		public LessonDetailsController(AbstractEntityManager<string, LessonDetail> entityManager) : base(entityManager)
		{
		}

		[HttpPost]
		[Route("upload/lessondetails")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}
	}
}