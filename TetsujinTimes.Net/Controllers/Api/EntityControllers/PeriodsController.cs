﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TetsujinTimes.Net.Controllers.Api.Abstracts;
using TetsujinTimes.Net.Models;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataManagers;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using System.Threading.Tasks;

namespace TetsujinTimes.Net.Controllers.Api
{
    public class PeriodsController :
		AbstractEntityController<string, Period>
	{
		public PeriodsController() : base(new PeriodsManager()) { }

		public PeriodsController(AbstractEntityManager<string, Period> entityManager) : base(entityManager)
		{
		}

		[HttpPost]
		[Route("upload/periods")]
		public new Task<IHttpActionResult> UploadAsync()
		{
			return base.UploadAsync();
		}

	}
}