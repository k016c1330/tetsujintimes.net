﻿
// 配列内から引数で指定したオブジェクトを削除します。
Array.prototype.remove = Array.prototype.remove || function (target) {
    this.splice(this.indexOf(target), 1);
};
