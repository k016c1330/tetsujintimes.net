﻿angular.module('App')
    .factory('Entity', ['$http', '$resource', '$timeout', function ($http, $resource, $timeout) {
        return function (path, idconfig) {
            var restPath, uploadPath;

            // パス名の取得
            if (angular.isArray(path)) {
                restPath = path[0];
                uploadPath = path[1];
            } else {
                restPath = path;
            }

            // エラー処理共通関数
            function error($scope, message, title) {
                // エラー通知を上位コントローラーへ送信
                $scope.$emit('error', message, title);
            }

            // インジケーター表示関数
            function showIndicator($scope, message) {
                $scope.$emit('showIndicator', message);
            }
            function hideIndicator($scope, message) {
                $scope.$emit('hideIndicator');
            }

            // Resourceオブジェクトの取得
            var Entity = $resource(restPath, idconfig, {
                update: { method: 'PUT' },
            });

            var entities = [];
            //var _currentParameter = undefined;
            // データ読み込み/検索
            this.query = function (parameter, $event, $scope) {
                entities = [];
                $scope.loading = true;
                Entity.query(parameter, function (results) {
                    // 配列を空に初期化
                    entities.splice(0, entities.length);
                    angular.forEach(results, function (item) {
                        entities.push(item);
                    });

                    $scope.loading = false;
                }, function (reason) {
                    // データ取得失敗
                    console.log(["読み込み失敗", reason]);
                    $scope.loading = false;
                });
                return entities;
            }

            // 新規作成
            this.create = function (item, $event, $scope) {
                console.log("create");

                // モーダルダイアログを表示
                var _this = this;
                var newItem = new Entity(item);
                // 現在のスコープのアイテム
                var entities = $scope.items;
                newItem.$save(function (result) {
                    // モーダルダイアログを非表示
                    $('#modal-create').modal('hide');
                    $scope.createErrorMessage = "";

                    // アイテムの追加をリストに反映
                    entities.push(result);
                    console.log(result);
                    result.$$created = true;

                    // 追加したデータの位置までスクロール
                    //_this.scroll(result);
                }, function (reason) {
                    // 新規作成失敗
                    //error($scope, reason.data.Message, "新規作成失敗");
                    $scope.createErrorMessage = reason.data.Message;
                });
            };

            // 指定したアイテムへスクロール
            this.scroll = function (item) {
                // モデルの更新を待つ
                $timeout(function () {
                    // アイテムを取得
                    var $parent = $('body');
                    var $target = $('#item_' + item.$$index);

                    /// スクロール位置を計算
                    var targetOffset = $target.offset().top;
                    var parentScrollTop = $parent.scrollTop()
                    var parentOffsetTop = $parent.offset().top;
                    var top = targetOffset + parentScrollTop - parentOffsetTop;

                    console.log(top, targetOffset, parentScrollTop, parentOffsetTop);
                    $parent.animate({ scrollTop: top }, 'fast', 'swing');
                });
            }

            // 更新
            this.update = function (item, $event, $scope) {
                console.log('update');

                // フォームの状態を取得
                var form = undefined;
                if ($scope) {
                    form = $scope.updateForm;
                }
                console.log(form);

                // 変更状態がなければ
                if (form && form.$pristine) {
                    console.log('変更がありません。');
                    return;
                }

                // アイテムを更新中に設定
                item.$$updating = true;

                // 更新処理を開始
                // 現在のスコープのアイテム
                var entities = $scope.items;
                //showIndicator($scope, '更新中…');
                item.$update(function (result) {
                    //hideIndicator($scope);
                    console.log(result);
                    // アイテムを更新済みに設定
                    result.$$updating = false;
                    result.$$updated = true;

                    // フォームの状態を更新
                    form && form.$setPristine();
                }, function (reason) {
                    hideIndicator($scope);
                    // 更新失敗時の処理
                    result.$$updating = false;
                    console.log($scope);
                    error($scope, reason.data.Message, '更新失敗');
                });
            };
            // 削除
            this.delete = function (item, $event, $scope) {
                console.log('delete');
                // 現在のスコープのアイテム
                var entities = $scope.items;
                item.$delete(function (result) {
                    entities.remove(item);
                    console.log(result);
                }, function (reason) {
                    error($scope, reason.data.Message, '削除失敗');
                });
            };

            // .xlsxファイルをアップロード
            function uploadXlsx(file, $scope) {
                var data = new FormData();
                data.append(file.name, file);


                showIndicator($scope, 'データ追加中…');
                $http.post(uploadPath, data, {
                    headers: { 'Content-Type': undefined },
                    transformRequest: null,
                }).then(function (result) {
                    hideIndicator($scope)
                    console.log(result);
                    var count = 0;
                    angular.forEach(result.data.Result.results, function (r) {
                        if (r.Result) {
                            var item = new Entity(r.Result);
                            // アイテムを追加
                            console.log(item);
                            entities.push(item);
                            count += 1;
                        }
                    });
                    console.log(count + " 個のデータを追加しました。");
                });
            }

            // Xlsxファイルのアップロード
            this.upload = function (form) {
                // jQueryで要素を取得　※非推奨
                var file = form.file.$$element.get(0).files[0];
                if (!file) {
                    console.log('ファイルが選択されていません。');
                    return;
                }
                uploadXlsx(file);
            };

            // ドラッグ&ドロップ
            this.dragover = function ($event, $scope) {
                $event.stopPropagation();
                $event.preventDefault();

                $event.originalEvent.dataTransfer.dropEffect = 'copy';
            };
            this.drop = function ($event, $scope) {
                $event.stopPropagation();
                $event.preventDefault();

                // 受け取ったファイルを取得
                var file = $event.originalEvent.dataTransfer.files[0];
                uploadXlsx(file, $scope);
            };
        }
    }])
    .service('DataStore', ['Entity', function (Entity) {
        this.teachers = new Entity(['/api/teachers/:id', '/upload/teachers'], { id: '@teacherId' });
        this.students = new Entity(['/api/students/:id', '/upload/students'], { id: '@studentId' });
        this.departments = new Entity(['/api/departments/:id', '/upload/departments'], { id: '@departmentCode' });
        this.classrooms = new Entity(['/api/classrooms/:id', '/upload/classrooms'], { id: '@classroomName' });
        this.periods = new Entity(['/api/periods/:id', '/upload/periods'], { id: '@number' });
        this.lessons = new Entity(['/api/lessons/:id', '/upload/lessons'], { id: '@id' });
        //this.timeTables = new Entity(['/api/timetables/:id', '/upload/timetables'], { id: '@id' });
        //this.attendLessons = new Entity(['/api/attendlessons/:id', '/upload/attendlessons'], { id: '@id' });
        //this.lessonDetails = new Entity(['/api/lessondetails/:id', '/upload/lessondetails'], { id: '@id' });
        //this.attendances = new Entity(['/api/attendances/:id', '/upload/attendances'], { id: '@id' });
        this.groups = new Entity(['/api/groups/:id', '/upload/groups'], { id: '@id' });
        this.attendGroups = new Entity(['/api/attendGroups/:id', '/upload/attendGroups'], { id: '@id' });
        this.lessonGroups = new Entity(['/api/lessonGroups/:id', '/upload/lessonGroups'], { id: '@id' });
        this.memos = new Entity(['/api/memos/:id', '/upload/memos'], { id: '@id' });
        
        // 新規作成モーダルを表示
        this.showCreateModal = function () {
            $('#modal-create').modal('show');
            $('#modal-create_first').focus();
        };
    }]);