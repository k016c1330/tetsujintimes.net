﻿/*
    ドラッグ＆ドロップ ディレクティブを登録
*/
angular.module('App')
    .directive('ngDrop', function ($parse) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {

                element.bind('drop', function (event) {
                    var fn = $parse(attrs.ngDrop);
                    $scope.$apply(function () {
                        fn($scope, { $event: event, $scope:$scope });
                    });
                });
            }
        };
    })
    .directive('ngDragover', function ($parse) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                element.bind('dragover', function (event) {
                    var fn = $parse(attrs.ngDragover);
                    $scope.$apply(function () {
                        element.addClass('ng-dragover');
                        fn($scope, { $event: event, $scope: $scope });
                    });
                });
                element.bind('dragleave', function (event) {
                    $scope.$apply(function () {
                        element.removeClass('ng-dragover');
                    });
                });
                element.bind('drop', function (event) {
                    $scope.$apply(function () {
                        element.removeClass('ng-dragover');
                    });
                });
            }
        };
    });