﻿angular.module('App')
    .config(['$locationProvider', function ($locationProvider) {
        //$locationProvider.hashPrefix('!');
        $locationProvider.html5Mode(true);
    }])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/Template/Home',
                controller: 'HomeController',
            })
            .when('/Login', {
                templateUrl: '/Template/Login',
                controller: 'LoginController',
            })
            .when('/Teachers', {
                templateUrl: '/Template/Teachers',
                controller: 'TeachersController',
            })
            .when('/Students', {
                templateUrl: '/Template/Students',
                controller: 'StudentsController',
            })
            .when('/Departments', {
                templateUrl: '/Template/Departments',
                controller: 'DepartmentsController',
            })
            .when('/Classrooms', {
                templateUrl: '/Template/Classrooms',
                controller: 'ClassroomsController',
            })
            .when('/Lessons', {
                templateUrl: '/Template/Lessons',
                controller: 'LessonsController',
            })
            .when('/Periods', {
                templateUrl: '/Template/Periods',
                controller: 'PeriodsController',
            })
            .when('/TimeTables', {
                templateUrl: '/Template/TimeTables',
                controller: 'TimeTablesController',
            })
            .when('/Groups', {
                templateUrl: '/Template/Groups',
                controller: 'GroupsController',
            })
            //.when('/AttendLessons', {
            //    templateUrl: '/Template/AttendLessons',
            //    controller: 'AttendLessonsController',
            //})
            .when('/Memos', {
                templateUrl: '/Template/Memos',
                controller: 'MemosController',
            })
            .when('/LessonDetails', {
                templateUrl: '/Template/LessonDetails',
                controller: 'LessonDetailsController',
            })
            .when('/Attendances', {
                templateUrl: '/Template/Attendances',
                controller: 'AttendancesController',
            })
            .when('/Notifications', {
                templateUrl: '/Template/Notifications',
                controller: 'NotificationsController',
            })
            .otherwise({
                redirectTo: '/'
            });
    });