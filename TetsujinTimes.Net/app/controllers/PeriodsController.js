﻿
angular.module('App')
    .controller('PeriodsController', ['$scope', '$resource', 'DataStore',
        function ($scope, $resource, DataStore) {
            $scope.store = DataStore;
            
            // 日付型コンバート関数
            $scope.formatToDate = function (date) {
                // 時刻を表す文字列をDate型オブジェクトに変換
                var result = new Date(0);
                var match = (/(\d{2})\:(\d{2})(?:\:(\d{2}))?/g).exec(date);
                if (match) {
                    var [hour, min, sec] = match.slice(1);
                    result.setHours(hour, min, sec || 0);

                    // 不正な日付だった場合は00:00:00を返す
                    if (result.toString() == "Invalid Date") result = new Date(0);
                }
                return result
            };
            $scope.formatFromDate = function (date) {
                // 不正な日付だった場合は処理終了
                if (!date)
                    return undefined;

                new Date().getMinutes
                var hour = ("0" + date.getHours()).slice(-2);
                var minute = ("0" + date.getMinutes()).slice(-2);
                var second = ("0" + date.getSeconds()).slice(-2);
                var result = hour + ":" + minute + ":" + second;
                return result;
            };
        }])
    ;
