﻿angular.module('App')
    .controller('LoginController', [
        '$scope', '$http', '$httpParamSerializerJQLike', '$log', '$q', '$location', '$cookies', 'DataStore',
        function ($scope, $http, $httpParamSerializerJQLike, $log, $q, $location, $cookies, DataStore) {
            $scope.store = DataStore;

            DataStore.user = undefined;

            //$scope.email = $scope.password = 'Administrator@admin.jp';
            $scope.saveLoginState = true;

            // ログイン処理
            $scope.login = function ($event, $scope) {
                $log.info($event);
                $log.info($scope);

                var data = {
                    grant_type: 'password',
                    username: $scope.email,
                    password: $scope.password,
                };
                var headers = {
                    'Content-Type': 'application/x-www-form-urlencoded',
                };
                $log.info(data);

                // ログイン処理
                $scope.errorMessage = 　'';
                $http({
                    url: '/token',
                    method: 'POST',
                    headers: headers,
                    transformRequest: $httpParamSerializerJQLike,
                    data: data
                }).then(function (result) {
                    // 成功時の処理
                    var data = result.data;
                    $log.info(data);

                    var token = DataStore.accessToken = data.access_token;

                    // チェックがついていればログイン状態をブラウザに記録する
                    if ($scope.saveLoginState) {
                        // Secure属性を付加
                        $cookies.put('access_token', token, {
                            //secure: true,
                        });
                    }

                    return $http({
                        url: 'api/account/userinfo',
                        method: 'GET',
                        headers: {
                            Authorization: 'Bearer ' + token,
                        },
                    });
                }, function (reason) {
                    // 失敗時の処理
                    return $q.reject(reason);
                }).then(function (resolve) {
                    // ユーザー情報を表示
                    var info = resolve.data.info;
                    $log.info(info);

                    // ユーザ情報を登録
                    DataStore.user = info;

                    $location.path('/');
                }, function (reject) {
                    $log.error(reject);
                    $scope.errorMessage = 'エラー：ログインに失敗しました';
                });
            };
        }])
    ;