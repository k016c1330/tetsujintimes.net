﻿angular.module('App')
    .controller('HomeController', ['$scope', '$route', '$location', '$http',
        function ($scope, $route, $location, $http) {

            // 全件取得のAPI
            var uploadPath = '/api/getxlsx';

            // ドラッグ&ドロップ
            $scope.dragover = function ($event, $scope) {

                $event.stopPropagation();
                $event.preventDefault();

                $event.originalEvent.dataTransfer.dropEffect = 'copy';
            };

            $scope.drop = function ($event, $scope) {

                $event.stopPropagation();
                $event.preventDefault();

                // 受け取ったファイルを取得
                var file = $event.originalEvent.dataTransfer.files[0];
                uploadXlsx(file, $scope);
            };

            // インジケーター表示関数
            function showIndicator($scope, message) {
                $scope.$emit('showIndicator', message);
            }
            function hideIndicator($scope, message) {
                $scope.$emit('hideIndicator');
            }

            // .xlsxファイルをアップロード
            function uploadXlsx(file, $scope) {
                var data = new FormData();
                data.append(file.name, file);


                showIndicator($scope, 'データ追加中…');
                $http.post(uploadPath, data, {
                    headers: { 'Content-Type': undefined },
                    transformRequest: null,
                }).then(function (result) {
                    hideIndicator($scope)
                    console.log(result);
                    var count = 0;
                    angular.forEach(result.data.Result, function (r) {
                        if (r.Result) {
                            var item = new Entity(r.Result);
                            // アイテムを追加
                            console.log(item);
                            entities.push(item);
                            count += 1;
                        }
                    });
                    console.log(count + " 個のデータを追加しました。");
                    $scope.$emit('notification', "データの登録が完了しました。", "インポート完了");
                });
            }
        }])
    ;