﻿angular.module('App')
    .controller('RootController', ['$scope', '$http', '$route', '$location', '$log', '$cookies', 'DataStore',
        function ($scope, $http, $route, $location, $log, $cookies, DataStore) {
            $scope.store = DataStore;

            // 現在のルーティングパスをスコープに通達
            $scope.location = $location;

            // 下部コントローラーから受け取ったエラー通知を表示
            $scope.$on('error', function (e, message, title) {
                $scope.errorMessage = message || "";
                $scope.errorTitle = title || "";

                // エラーダイアログを表示
                var log = $('#modal-error').modal('show');
                $('#modal-error_fast').focus();
            });

            // 下部コントローラーから受け取った情報通知を表示
            $scope.$on('notification', function (e, message, title) {
                $scope.notificationMessage = message || "";
                $scope.notificationTitle = title || "";

                // エラーダイアログを表示
                var log = $('#modal-notification').modal('show');
                $('#modal-notification_fast').focus();
            });

            // ログアウト処理
            $scope.logout = function () {
                // ログイン情報を消去
                DataStore.user = undefined;
                DataStore.accessToken = undefined;
                $cookies.put('access_token', undefined);
                $location.path('/Login');
            }

            // 更新用インジケーターを表示
            $scope.$on('showIndicator', function (e, message) {
                $('.indicator').addClass('show');
                $('.indicator .indicator-title').text(message);
            });
            $scope.$on('hideIndicator', function (e) {
                $('.indicator').removeClass('show');
            });




            // 現在のパスを保存
            //var path = $location.path();

            // クッキーがあればユーザ情報を取得
            var token = DataStore.accessToken = $cookies.get('access_token');
            $log.info('DataStore: token = \'' + token + '\'');
            if (token) {
                // 読み込みが終わるまで仮データを登録
                DataStore.user = {
                    name: '認証中…',
                };
                $http({
                    url: 'api/account/userinfo',
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + token,
                    },
                }).then(function (resolve) {
                    // ユーザー情報を表示
                    var info = resolve.data.info;
                    $log.info(resolve, info);

                    // ユーザ情報を登録
                    DataStore.user = info;

                    if (!DataStore.user) {
                        // ユーザ情報が存在しない場合はログインページに遷移
                        $location.path('/Login');
                    }

                    //$location.path(path);
                }, function (reject) {
                    $log.error(reject);
                    DataStore.user = undefined;
                    DataStore.accessToken = undefined;

                    $location.path('/Login');
                });
            }
            if (!DataStore.user) {
                // ユーザ情報が存在しない場合はログインページに遷移
                $location.path('/Login');
            }
        }])
    ;