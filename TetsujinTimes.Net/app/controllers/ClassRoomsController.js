﻿angular.module('App')
    .controller('ClassroomsController', ['$scope', '$resource', 'DataStore',
        function ($scope, $resource, DataStore) {
            $scope.store = DataStore;
        }])
    ;