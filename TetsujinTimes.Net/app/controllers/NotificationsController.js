﻿angular.module('App')
    .controller('NotificationsController', ['$scope', '$http', '$resource', 'DataStore',
        function ($scope, $http, $resource, DataStore) {
            // 教師情報を取得
            $resource('/api/teachers/:id', { 'id': '@id' })
                .query(function (teachers) {
                    $scope.teachers = teachers;
                    console.log(teachers);
                });

            // 認証ヘッダを作成
            var headers = {};
            var token = DataStore.accessToken;
            if (token) {
                headers.Authorization = 'Bearer ' + token;
            }

            $scope.create = function (item) {
                console.log('create:');
                $http({
                    method: 'POST',
                    url: '/api/notifications',
                    data: item,
                    headers: headers,
                }).then(function (data, status, headers, config) {
                    console.log(data, status, headers, config);
                });
            }
        }]);