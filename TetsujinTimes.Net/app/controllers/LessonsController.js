﻿angular.module('App')
    .controller('LessonsController', ['$scope', '$resource', 'DataStore',
        function ($scope, $resource, DataStore) {
            $scope.store = DataStore;

            //// 教室情報を取得
            //$resource('/api/classrooms/:id', { 'id': '@id' })
            //    .query(function (classrooms) {
            //        $scope.classrooms = classrooms;
            //        console.log(classrooms);
            //    });
            //// 教師情報を取得
            //$resource('/api/teachers/:id', { 'id': '@id' })
            //    .query(function (teachers) {
            //        $scope.teachers = teachers;
            //        console.log(teachers);
            //    });
            //// 時限情報を取得
            //$resource('/api/periods/:id', { 'id': '@id' })
            //    .query(function (periods) {
            //        $scope.periods = periods;
            //        console.log(periods);
            //    });

            //// 授業一覧を取得
            //var Lessons = $resource('/api/lessons/:id', { 'id': '@id' }, {
            //    update: {
            //        method: 'PUT',
            //    },
            //});
            //Lessons.query(function (results) {
            //    $scope.items = results;
            //});

            //$scope.create = function (item) {
            //    console.log("create");
            //    var newItem = new Lessons(item);
            //    newItem.$save(function (result) {
            //        console.log(result);
            //        if (!$scope.items) {
            //            $scope.items = [];
            //        }
            //        $scope.items.push(newItem);
            //    });
            //};
            //$scope.update = function (item) {
            //    console.log("update");
            //    item.$update(function (result) {
            //        console.log(result);
            //    });
            //};

            //$scope.delete = function (item) {
            //    console.log("delete")
            //    item.$delete(function (result) {
            //        $scope.items.remove(item);
            //        console.log(result);
            //    })
            //};

            //$scope.deleteAll = function () {
            //    angular.forEach($scope.items, function (item, i) {
            //        if (item.checked) {
            //            // チェック項目がついている要素を全削除
            //            $scope.delete(item);
            //        }
            //    });
            //};

            // 日付型コンバート関数
            $scope.formatToDate = function (date) {
                // 文字列を日付に変換
                var result = new Date(date);
                // 不正な日付だった場合は現在の日付を使用
                if (result.toString() === "Invalid Date") result = new Date();
                return result
            };
            $scope.formatFromDate = function (date) {
                // 不正な日付だった場合は処理終了
                if (!date)
                    return undefined;

                var year = ("0" + date.getFullYear()).slice(-2);
                var month = ("0" + (date.getMonth() + 1)).slice(-2);
                var day = ("0" + date.getDate()).slice(-2);
                var result = year + "/" + month + "/" + day;
                return result;
            };
        }])
