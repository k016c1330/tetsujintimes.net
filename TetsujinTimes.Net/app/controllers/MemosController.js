﻿angular.module('App')
    .controller('MemosController', ['$scope', '$resource', 'DataStore',
        function ($scope, $resource, DataStore) {
            $scope.store = DataStore;
            
            $scope.convertToDate = function (date) {
                var result = Date.parse(date);
                if (isNaN(result)) {
                    return null;
                }
                return result;
            };
        }])
    ;