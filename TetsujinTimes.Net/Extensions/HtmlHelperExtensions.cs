﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace TetsujinTimes.Net.Extensions
{
	public static class HtmlHelperExtensions
	{
		public static IHtmlString EnumDisplayFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
		{
			var value = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;
			if (!Enum.IsDefined(typeof(TEnum), value))
			{
				return new HtmlString(value.ToString());
			}

			var field = value.GetType().GetField(value.ToString());
			var attributes = field.GetCustomAttributes(typeof(DisplayAttribute), false) as DisplayAttribute[];
			if (attributes != null && attributes.Length != 0)
			{
				return new HtmlString(attributes[0].Name);
			}
			else
			{
				return new HtmlString(value.ToString());
			}
		}
	}
}