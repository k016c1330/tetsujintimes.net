﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetsujinTimes.Net.Extensions
{
	public static class ParameterExtensions
	{
		/// <summary>
		/// パラメータから整数値を取得
		/// </summary>
		/// <param name="parameters"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool TryGetInteger(this IDictionary<string, string> parameters, string key, out int value)
		{
			value = 0;
			return parameters.TryGetValue(key, out string valueString) && int.TryParse(valueString, out value);
		}

		/// <summary>
		/// パラメータから文字列を取得
		/// </summary>
		/// <param name="parameters"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool TryGetString(this IDictionary<string, string> parameters, string key, out string value)
		{
			return parameters.TryGetValue(key, out value);
		}
	}
}