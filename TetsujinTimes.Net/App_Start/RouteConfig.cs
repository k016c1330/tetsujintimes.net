﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TetsujinTimes.Net
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			// angularjsでのルーティングを有効化するための処理
			routes.MapRoute(
				name: "Template",
				url: "Template/{viewName}",
				defaults: new { controller = "Template", action = "Default", viewName = UrlParameter.Optional }
			);

			// 通常のコントローラーへは/web/...のようにアクセス
			routes.MapRoute(
				name: "Web",
				url: "web/{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);

			// ルート外のURLはメインページへ移動
			routes.MapRoute(
				name: "Default",
				url: "{*.}",
				defaults: new { controller = "Home", action = "Index" }
			);


			//routes.MapRoute(
			//	name: "Default",
			//	url: "{controller}/{action}/{id}",
			//	defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			//);
		}
	}
}
