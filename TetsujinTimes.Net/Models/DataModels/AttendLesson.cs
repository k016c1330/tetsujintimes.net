﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("受講")]
	public class AttendLesson : AbstractEntity
	{
		[DataMember]
		[Required]
		[Index("LessonAndStudent", 1, IsUnique = true)]
		[DisplayName("授業ID")]
		[JsonProperty("lessonId")]
		public int LessonId { get; set; }

		[DataMember]
		[Required]
		[Index("LessonAndStudent", 2, IsUnique = true)]
		[DisplayName("生徒ID")]
		[JsonProperty("studentId")]
		public string StudentId { get; set; }

		public virtual Lesson Lesson { get; set; }

		public virtual Student Student { get; set; }

		[DataMember]
		[DisplayName("授業名")]
		[JsonProperty("lessonName")]
		public string LessonName => Lesson?.LessonName;

		[DataMember]
		[DisplayName("生徒名")]
		[JsonProperty("studentName")]
		public string StudentName => Student?.Name;

		[DataMember]
		[WorksheetColumn]
		[DisplayName("授業コード")]
		[JsonProperty("lessonCode")]
		public string LessonCode => Lesson?.LessonCode;

		[DataMember]
		[WorksheetColumn]
		[DisplayName("学籍番号")]
		[JsonProperty("studentIdNumber")]
		public string StudentIdNumber => Student?.StudentId;

	}

}