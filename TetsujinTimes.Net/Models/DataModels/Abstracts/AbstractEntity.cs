﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Web;

namespace TetsujinTimes.Net.Models.DataModels.Abstracts
{
	/// <summary>
	/// データ実体の共通属性
	/// </summary>
	[DataContract]
	public abstract class AbstractEntity : IIdentifiable
	{
		[DataMember]
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[JsonProperty("id")]
		public int Id { get; set; }

		public virtual object Identity => Id;
	}
}