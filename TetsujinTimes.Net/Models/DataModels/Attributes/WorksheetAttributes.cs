﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetsujinTimes.Net.Models.DataModels.Attributes
{
	/// <summary>
	/// ワークシートのカラム属性を設定します。
	/// </summary>
	public class WorksheetColumnAttribute : Attribute
	{
		public int Index { get; set; } = int.MaxValue;
		public WorksheetColumnAttribute()
		{
		}
	}
}