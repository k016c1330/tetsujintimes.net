﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("教室")]
	public class Classroom : AbstractEntity
	{
		[DataMember]
		[WorksheetColumn]
		[Required]
		[Index(IsUnique = true)]
		[StringLength(5)]
		[DisplayName("教室名")]
		[JsonProperty("classroomName")]
		public string ClassroomName { get; set; }

		public override object Identity => this.ClassroomName;
	}

}