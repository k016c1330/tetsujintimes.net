﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetsujinTimes.Net.Models.DataModels
{
	public interface IIdentifiable
	{
		object Identity { get; }
	}
}