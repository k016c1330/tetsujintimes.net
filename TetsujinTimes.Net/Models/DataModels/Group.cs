﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("グループ")]
	public class Group : AbstractEntity
	{
		[DataMember]
		[WorksheetColumn]
		[Required]
		//[Index(IsUnique =true)]
		[DisplayName("グループコード")]
		[JsonProperty("groupCode")]
		public string GroupCode { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DisplayName("説明")]
		[JsonProperty("description")]
		public string Description { get; set; }
	}

	[DisplayName("グループ所属生徒")]
	public class AttendGroup : AbstractEntity
	{
		[DataMember]
		[Required]
		[Index("GroupAndStudent", 1, IsUnique = true)]
		[DisplayName("グループID")]
		[JsonProperty("groupId")]
		public int GroupId { get; set; }

		[DataMember]
		[Required]
		[Index("GroupAndStudent", 2, IsUnique = true)]
		[DisplayName("生徒ID")]
		[JsonProperty("studentId")]
		public string StudentId { get; set; }
		
		public virtual Group Group {get;set;}
		public virtual Student Student {get;set;}
		
		[DataMember]
		[WorksheetColumn(Index = 1)]
		[DisplayName("グループコード")]
		[JsonProperty("groupCode")]
		public string GroupCode => Group?.GroupCode;

		[DataMember]
		[WorksheetColumn(Index = 2)]
		[DisplayName("学籍番号")]
		[JsonProperty("studentIdNumber")]
		public string StudentIdNumber => Student?.StudentId;

		[DataMember]
		[WorksheetColumn(Index = 3)]
		[DisplayName("氏名")]
		[JsonProperty("studentName")]
		public string StudentName => Student?.Name;

		[DataMember]
		[WorksheetColumn(Index = 4)]
		[DisplayName("説明")]
		[JsonProperty("description")]
		public string description => Group?.Description;
	}

	[DisplayName("グループ履修授業")]
	public class LessonGroup : AbstractEntity
	{
		[DataMember]
		[Required]
		[Index("GroupAndLesson", 1, IsUnique = true)]
		[DisplayName("グループID")]
		[JsonProperty("groupId")]
		public int GroupId { get; set; }

		[DataMember]
		[Required]
		[Index("GroupAndLesson", 2, IsUnique = true)]
		[DisplayName("授業ID")]
		[JsonProperty("lessonId")]
		public int LessonId { get; set; }


		public virtual Group Group { get; set; }
		public virtual Lesson Lesson { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 1)]
		[DisplayName("グループコード")]
		[JsonProperty("groupCode")]
		public string GroupCode => Group?.GroupCode;

		[DataMember]
		[WorksheetColumn(Index = 3)]
		[DisplayName("説明")]
		[JsonProperty("description")]
		public string Description => Group?.Description;

		[DataMember]
		[WorksheetColumn(Index = 2)]
		[DisplayName("授業コード")]
		[JsonProperty("lessonCode")]
		public string LessonCode => Lesson?.LessonCode;
	}
}