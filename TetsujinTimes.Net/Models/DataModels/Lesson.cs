﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	public enum TetsujinSeason
	{
		[Display(Name = "前期")]
		Previous,

		[Display(Name = "後期")]
		Later,
	}

	[DisplayName("授業")]
	public class Lesson : AbstractEntity
	{
		[DataMember]
		[WorksheetColumn(Index = 1)]
		[Required]
		[MaxLength(13)]
		[MinLength(13)]
		[Index(IsUnique = true)]
		[DisplayName("授業コード")]
		[JsonProperty("lessonCode")]
		public string LessonCode { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 2)]
		[Required]
		[DisplayName("授業名")]
		[JsonProperty("lessonName")]
		public string LessonName { get; set; }

		[DataMember]
		[Required]
		[DisplayName("担任ID")]
		[JsonProperty("teacherId")]
		public string TeacherId { get; set; }

		[DataMember]
		[Required]
		[DisplayName("教室ID")]
		[JsonProperty("classroomId")]
		public int ClassroomId { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 13)]
		[Required]
		[EnumDataType(typeof(DayOfWeek))]
		[DisplayName("曜日")]
		[JsonProperty("weekDay")]
		[JsonConverter(typeof(WeekDayConverter))]
		public DayOfWeek WeekDay { get; set; } = DayOfWeek.Monday;
		
		[DataMember]
		[Required]
		[DisplayName("開始時限ID")]
		[JsonProperty("startPeriodId")]
		public int StartPeriodId { get; set; }

		[DataMember]
		[Required]
		[DisplayName("終了時限ID")]
		[JsonProperty("endPeriodId")]
		public int EndPeriodId { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 10)]
		[Required]
		[DisplayName("前期/後期")]
		[JsonProperty("season")]
		public TetsujinSeason Season { get; set; }

		[DataMember]
		[WorksheetColumn]
		[DataType(DataType.MultilineText)]
		[DisplayName("説明")]
		[JsonProperty("description")]
		public string Description { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 11)]
		[DataType(DataType.Date)]
		[DisplayName("授業開始日")]
		[DisplayFormat(DataFormatString = @"{0:yyyy/mm/dd}", ApplyFormatInEditMode = true)]
		[JsonProperty("firstDate")]
		[JsonConverter(typeof(DateJsonConverter))]
		public DateTime FirstDate { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 12)]
		[DisplayName("授業数")]
		[JsonProperty("totalCount")]
		public int TotalCount { get; set; }


		public virtual Teacher Teacher { get; set; }

		public virtual Classroom Classroom { get; set; }

		public virtual Period StartPeriod { get; set; }

		public virtual Period EndPeriod { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 3)]
		[DisplayName("担任ID")]
		[JsonProperty("teacherIdNumber")]
		public int? TeacherIdNumber => Teacher?.TeacherId;

		[DataMember]
		[WorksheetColumn(Index = 4)]
		[DisplayName("担任名")]
		[JsonProperty("teacherName")]
		public string TeacherName => Teacher?.Name;

		[DataMember]
		[WorksheetColumn(Index = 14)]
		[DisplayName("開始時限")]
		[JsonProperty("startPeriod")]
		public int? StartPeriodNumber => StartPeriod?.Number;

		[DataMember]
		[WorksheetColumn(Index = 15)]
		[DisplayName("終了時限")]
		[JsonProperty("endPeriod")]
		public int? EndPeriodNumber => EndPeriod?.Number;

		[DataMember]
		[DisplayName("開始時刻")]
		[JsonProperty("startTime")]
		public TimeSpan? StartTime => StartPeriod?.StartTime;

		[DataMember]
		[DisplayName("終了時刻")]
		[JsonProperty("endTime")]
		public TimeSpan? EndTime => StartPeriod?.EndTime;

		[DataMember]
		[WorksheetColumn]
		[DisplayName("教室名")]
		[JsonProperty("classroomName")]
		public string ClassroomName => Classroom?.ClassroomName;

		[DataMember]
		[DisplayName("曜日番号")]
		[JsonProperty("weekDayNumber")]
		public byte WeekDayNumber => (byte)(this.WeekDay + 1);
	}
	
	// Jsonに変換する場合はDayOfWeek列挙体名を値として使用
	class WeekDayConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DayOfWeek);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var obj = serializer.Deserialize(reader);
			if (obj is String weekDayString)
			{
				if (Enum.TryParse<DayOfWeek>(weekDayString, out DayOfWeek weekDay))
				{
					return weekDay;
				}
			}
			throw new ArgumentException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value is DayOfWeek weekDay)
			{
				JToken.FromObject(Enum.GetName(typeof(DayOfWeek), weekDay)).WriteTo(writer);
			}
			else
			{
				JToken.FromObject(value).WriteTo(writer);
			}
		}
	}

	/// <summary>
	/// Date変換用JsonConverter実装クラス
	/// </summary>
	class DateJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.Value is string dateString)
			{
				if (DateTime.TryParse(dateString, out DateTime date))
				{
					return date;
				}
			}
			throw new ArgumentException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value is DateTime date)
			{
				JToken.FromObject(date.ToShortDateString()).WriteTo(writer);
			}
			else
			{
				JToken.FromObject(value).WriteTo(writer);
			}
		}
	}
}