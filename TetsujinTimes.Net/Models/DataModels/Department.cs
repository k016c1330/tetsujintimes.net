﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("学科")]
	public class Department : AbstractEntity
	{
		[DataMember]
		[WorksheetColumn]
		[Required]
		[MaxLength(2)]
		[MinLength(2)]
		[Index(IsUnique = true)]
		[DisplayName("学科コード")]
		[JsonProperty("departmentCode")]
		public string DepartmentCode { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DisplayName("学科名")]
		[JsonProperty("departmentName")]
		public string DepartmentName { get; set; }

		public override object Identity => this.DepartmentCode;
	}
}