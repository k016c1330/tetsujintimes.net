﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{

	[DisplayName("時間割")]
	public class TimeTable : AbstractEntity
	{
		[DataMember]
		[Required]
		[Index("Department", 1, IsUnique = true)]
		[DisplayName("授業ID")]
		[JsonProperty("lessonId")]
		public int LessonId { get; set; }

		[DataMember]
		[Required]
		[Index("Department", 2, IsUnique = true)]
		[DisplayName("学科ID")]
		[JsonProperty("departmentId")]
		public int DepartmentId { get; set; }

		[WorksheetColumn(Index = 1)]
		[DisplayName("授業コード")]
		[JsonProperty("lessonCode")]
		public string LessonCode => Lesson?.LessonCode;

		[WorksheetColumn(Index = 2)]
		[DisplayName("学科コード")]
		[JsonProperty("departmentCode")]
		public string DepartmentCode => Department?.DepartmentCode;

		[DataMember]
		[WorksheetColumn(Index = 3)]
		[Required]
		[Index("Department", 3, IsUnique = true)]
		[DisplayName("学年")]
		[JsonProperty("grade")]
		public int Grade { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 4)]
		[Required]
		[Index("Department", 4, IsUnique = true)]
		[DisplayName("組")]
		[JsonProperty("classNum")]
		public int ClassNum { get; set; }
		
		public virtual Lesson Lesson { get; set; }
		
		public virtual Department Department { get; set; }
	}
}