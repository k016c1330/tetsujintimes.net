﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("メモ")]
	public class Memo : AbstractEntity
	{
		[DataMember]
		[Required]
		[Index("LessonAndStudent", 1, IsUnique = true)]
		[DisplayName("授業ID")]
		[JsonProperty("lessonId")]
		public int LessonId { get; set; }
		
		[DataMember]
		[Required]
		[Index("LessonAndStudent", 2, IsUnique = true)]
		[DisplayName("生徒ID")]
		[JsonProperty("studentId")]
		public string StudentId { get; set; }
		
		[DataMember]
		[WorksheetColumn(Index = 5)]
		[DataType(DataType.DateTime)]
		[DisplayName("作成日時")]
		[DisplayFormat(DataFormatString = @"{0:yyyy/MM/dd hh\:mm}", ApplyFormatInEditMode = true)]
		[JsonProperty("memoAt")]
		public DateTime MemoAt { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 6)]
		[Required]
		[DataType(DataType.MultilineText)]
		[DisplayName("内容")]
		[JsonProperty("content")]
		public string Content { get; set; }
		
		public virtual Lesson Lesson { get; set; }
		
		public virtual Student Student { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 2)]
		[DisplayName("授業名")]
		[JsonProperty("lessonName")]
		public string LessonName => Lesson?.LessonName;

		[DataMember]
		[WorksheetColumn(Index = 1)]
		[DisplayName("授業コード")]
		[JsonProperty("lessonCode")]
		public string LessonCode => Lesson?.LessonCode;

		[DataMember]
		[WorksheetColumn(Index = 4)]
		[DisplayName("生徒名")]
		[JsonProperty("studentName")]
		public string StudentName => Student?.Name;

		[DataMember]
		[WorksheetColumn(Index = 3)]
		[DisplayName("学籍番号")]
		[JsonProperty("studentIdNumber")]
		public string StudentIdNumber => Student?.StudentId;
	}

}