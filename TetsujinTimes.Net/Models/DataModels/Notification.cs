﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("通知情報")]
	public class Notification : AbstractEntity
	{
		[Required]
		[DataMember]
		[DisplayName("タイトル")]
		[JsonProperty("title")]
		public string Title { get; set; }

		[Required]
		[DataMember]
		[DisplayName("内容")]
		[JsonProperty("description")]
		public string Description { get; set; }

		//[Required]
		//[DataMember]
		//[DisplayName("送信者")]
		//[JsonProperty("teacherId")]
		//public string TeacherId { get; set; }
		
		[DisplayName("送信日時")]
		[DataMember]
		[JsonProperty("sendDate")]
		public DateTime SendDate { get; set; }

		[Required]
		[DataMember]
		[DisplayName("送信先")]
		public string SendTo { get; set; }

		//public virtual Teacher Teacher { get; set; }
		
		//[DataMember]
		//[DisplayName("送信者")]
		//[JsonProperty("TeacherName")]
		//public string TeacherName => Teacher?.Name;
	}

	[DisplayName("通知登録")]
	public class NotificationLinkStudent : AbstractEntity
	{
		[DataMember]
		[Required]
		[Index(IsUnique = true)]
		[DisplayName("生徒ID")]
		[JsonProperty("StudentId")]
		public string StudentId { get; set; }

		[DataMember]
		[Required]
		[DisplayName("登録Id")]
		public string RegistrationId { get; set; }

		public virtual Student Student { get; set; }

		[DataMember]
		[DisplayName("生徒ID")]
		[JsonProperty("StudentIdNumber")]
		public string StudentIdNumber => Student?.StudentId;
	}
}