﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	// ApplicationUser クラスにさらにプロパティを追加すると、ユーザーのプロファイル データを追加できます。詳細については、https://go.microsoft.com/fwlink/?LinkID=317594 を参照してください。
	[DataContract]
	[DisplayColumn("UserName")]
	public abstract class ApplicationUser : IdentityUser, IIdentifiable
	{
		[DataMember]
		[JsonProperty("id")]
		public override string Id { get => base.Id; set => base.Id = value; }
		
		[DataMember]
		[WorksheetColumn(Index = 3)]
		[DisplayName("メールアドレス")]
		[JsonProperty("email")]
		public override string Email { get => base.Email;
			set
			{
				base.UserName = value;
				base.Email = value;
			}
		}
		
		[DataMember]
		[WorksheetColumn(Index = 2)]
		[DisplayName("氏名")]
		[JsonProperty("name")]
		public string Name { get; set; }

		[DataMember]
		[JsonProperty("userType")]
		public abstract string UserType { get; }

		public virtual object Identity => Id;

		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
		{
			return await GenerateUserIdentityAsync(manager, DefaultAuthenticationTypes.ApplicationCookie);
		}

		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
		{
			// authenticationType が CookieAuthenticationOptions.AuthenticationType で定義されているものと一致している必要があります
			var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
			// ここにカスタム ユーザー クレームを追加します

			return userIdentity;
		}
	}
	
	[DisplayName("教師")]
	public class Teacher : ApplicationUser
	{
		[DataMember]
		[WorksheetColumn(Index = 1)]
		[Required]
		[Index("UserId", Order = 0, IsUnique = true)]	// 教師/生徒共通のユニークキー
		[DisplayName("教師ID")]
		[DisplayFormat(DataFormatString = "{0:00000}")]
		[JsonProperty("teacherId")]
		public int TeacherId { get; set; }
		
		public override string UserType => nameof(Teacher);

		public override object Identity => this.TeacherId;
	}
	
	[DisplayName("生徒")]
	public class Student : ApplicationUser
	{
		[DataMember]
		[WorksheetColumn(Index = 1)]
		[Required]
		[MaxLength(9)]
		[MinLength(9)]
		[Index("UserId", Order=1, IsUnique = true)] // 教師/生徒共通のユニークキー
		[DisplayName("学籍番号")]
		[JsonProperty("studentId")]
		public string StudentId { get; set; }
		
		[DataMember]
		[Required]
		[DisplayName("学科ID")]
		[JsonProperty("departmentId")]
		public int DepartmentId { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 11)]
		[Required]
		[DisplayName("学年")]
		[JsonProperty("grade")]
		public int Grade { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 12)]
		[Required]
		[DisplayName("組")]
		[JsonProperty("classNum")]
		public int ClassNum { get; set; }
		
		public virtual Department Department { get; set; }

		[DataMember]
		[WorksheetColumn(Index = 10)]
		[DisplayName("学科コード")]
		[JsonProperty("departmentCode")]
		public string DepartmentCode => Department?.DepartmentCode;

		[DataMember]
		[DisplayName("学科名")]
		[JsonProperty("departmentName")]
		public string DepartmentName => Department?.DepartmentName;

		public override string UserType => nameof(Student);
		public override object Identity => this.StudentId; 
	}
}