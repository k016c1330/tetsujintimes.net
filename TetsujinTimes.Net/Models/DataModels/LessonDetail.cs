﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("授業詳細")]
	public class LessonDetail : AbstractEntity
	{
		[DataMember]
		[WorksheetColumn]
		[DisplayName("授業ID")]
		[JsonProperty("lessonId")]
		public int LessonId { get; set; }


		[DataMember]
		[WorksheetColumn]
		[DisplayName("授業コード")]
		[JsonProperty("lessonCode")]
		public string LessonCode => Lesson?.LessonCode;

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DisplayName("授業数")]
		[JsonProperty("lessonCount")]
		public int LessonCount { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = @"{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
		[DisplayName("日付")]
		[JsonProperty("date")]
		public DateTime Date { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DataType(DataType.Time)]
		[DisplayName("開始時刻")]
		[DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
		[JsonProperty("startTime")]
		public TimeSpan StartTime { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DataType(DataType.Time)]
		[DisplayName("終了時刻")]
		[DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
		[JsonProperty("endTime")]
		public TimeSpan EndTime { get; set; }

		[DataMember]
		[WorksheetColumn]
		[DataType(DataType.MultilineText)]
		[DisplayName("説明")]
		[JsonProperty("description")]
		public string Description { get; set; }

		public virtual Lesson Lesson { get; set; }
	}
}