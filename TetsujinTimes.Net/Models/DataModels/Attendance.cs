﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("出席情報")]
	public class Attendance : AbstractEntity
	{

		[DataMember]
		[WorksheetColumn]
		[Required]
		[Index(IsUnique = true)]
		[Index("LessonDetailToStudent", 0, IsUnique = true)]
		[DisplayName("授業詳細ID")]
		[JsonProperty("lessonDetailId")]
		public int LessonDetailId { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[Index("LessonDetailToStudent", 1, IsUnique = true)]
		[DisplayName("履修ID")]
		[JsonProperty("attendLessonId")]
		public int AttendLessonId { get; set; }

		[DataMember]
		[WorksheetColumn]
		[DataType(DataType.Time)]
		[DisplayName("出席時刻")]
		[DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
		[JsonProperty("attendanceTime")]
		public TimeSpan AttendanceTime { get; set; }
		
		[DataMember]
		[WorksheetColumn]
		[Required]
		[DisplayName("出席")]
		[JsonProperty("attended")]
		public bool Attended { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DisplayName("遅刻")]
		[JsonProperty("wasLate")]
		public bool WasLate { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DisplayName("早退")]
		[JsonProperty("leftEarly")]
		public bool LeftEarly { get; set; }
		
		public LessonDetail LessonDetail { get; set; }
		public AttendLesson AttendLesson { get; set; }
	}
}