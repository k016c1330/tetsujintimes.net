﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.DataModels
{
	[DisplayName("時限")]
	public class Period : AbstractEntity
	{
		[DataMember]
		[WorksheetColumn]
		[Required]
		[Index(IsUnique = true)]
		[DisplayName("時限")]
		[JsonProperty("number")]
		public int Number { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DataType(DataType.Time)]
		[DisplayName("開始時刻")]
		[DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
		[JsonProperty("startTime")]
		public TimeSpan StartTime { get; set; }

		[DataMember]
		[WorksheetColumn]
		[Required]
		[DataType(DataType.Time)]
		[DisplayName("終了時刻")]
		[DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
		[JsonProperty("endTime")]
		public TimeSpan EndTime { get; set; }
	}
}