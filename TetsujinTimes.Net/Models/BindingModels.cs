﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Models
{
	public abstract class UserBindingModelBase
	{
		[Required]
		[Display(Name = "氏名")]
		public string Name { get; set; }

		[Required]
		[Display(Name = "電子メール")]
		[EmailAddress]
		public string Email { get; set; }
	}

	public abstract class TeacherBindingModelBase : UserBindingModelBase
	{
		[Required]
		[Display(Name = "教師ID")]
		public int TeacherId { get; set; }

		public TeacherBindingModelBase() { }

		public TeacherBindingModelBase(Teacher teacher)
		{
			Name = teacher.Name;
			Email = teacher.Email;
			TeacherId = teacher.TeacherId;
		}

		public Teacher Create() => new Teacher
		{
			Name = Name,
			Email = Email,
			TeacherId = TeacherId,
		};
	}

	public class StudentBindingModelBase : UserBindingModelBase
	{
		[Required]
		[Display(Name = "学籍番号")]
		[StringLength(9, ErrorMessage = "{0} の長さは、{2} 文字である必要があります。", MinimumLength = 9)]
		public string StudentId { get; set; }

		[Required]
		[Display(Name = "学科ID")]
		public int DepartmentId { get; set; }

		[Required]
		[Display(Name = "学年")]
		public int Grade { get; set; }

		[Required]
		[Display(Name = "組")]
		public int ClassNum { get; set; }

		public StudentBindingModelBase() { }

		public StudentBindingModelBase(Student student)
		{
			Name = student.Name;
			Email = student.Email;
			StudentId = student.StudentId;
			DepartmentId = student.DepartmentId;
			Grade = student.Grade;
			ClassNum = student.ClassNum;
		}

		public Student Create() => new Student
		{
			Name = Name,
			Email = Email,
			StudentId = StudentId,
			DepartmentId = DepartmentId,
			Grade = Grade,
			ClassNum = ClassNum,
		};
	}
	public class CreateStudentBindingModel : StudentBindingModelBase
	{
		//[Required]
		[StringLength(100, ErrorMessage = "{0} の長さは、{2} 文字以上である必要があります。", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "パスワード")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "パスワードの確認入力")]
		[Compare("Password", ErrorMessage = "パスワードと確認のパスワードが一致しません。")]
		public string ConfirmPassword { get; set; }

		public CreateStudentBindingModel() { }
		public CreateStudentBindingModel(Student student) : base(student) { }
	}

	public class UpdateStudentBindingModel : StudentBindingModelBase
	{
		public UpdateStudentBindingModel() { }
		public UpdateStudentBindingModel(Student student) : base(student) { }
	}

	public class CreateTeacherBindingModel : TeacherBindingModelBase
	{
		//[Required]
		[StringLength(100, ErrorMessage = "{0} の長さは、{2} 文字以上である必要があります。", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "パスワード")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "パスワードの確認入力")]
		[Compare("Password", ErrorMessage = "パスワードと確認のパスワードが一致しません。")]
		public string ConfirmPassword { get; set; }

		public CreateTeacherBindingModel() { }
		public CreateTeacherBindingModel(Teacher teacher) : base() { }
	}

	public class UpdateTeacherBindingModel : TeacherBindingModelBase
	{
		public UpdateTeacherBindingModel() { }
		public UpdateTeacherBindingModel(Teacher teacher) : base(teacher) { }
	}

	public static class BindingModelUtil
	{
		
		public static void UpdateFromBindingModel(this Student student, StudentBindingModelBase model)
		{
			student.Name = model.Name;
			student.Email = model.Email;
			student.StudentId = model.StudentId;
			student.DepartmentId = model.DepartmentId;
			student.Grade = model.Grade;
			student.ClassNum = model.ClassNum;
		}

		public static void UpdateFromBindingModel(this Teacher teacher, TeacherBindingModelBase model)
		{
			teacher.Name = model.Name;
			teacher.Email = model.Email;
			teacher.TeacherId = model.TeacherId;
		}
	}
}