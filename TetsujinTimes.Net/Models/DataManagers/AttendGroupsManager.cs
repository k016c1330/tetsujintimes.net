﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class AttendGroupsManager :
		AbstractEntityManager<string, AttendGroup>
	{
		protected override AttendGroup CreateFromJObject(JObject jObject)
		{
			var db = new ApplicationDbContext();
			var groupCode = jObject.GetValue("groupCode")?.Value<string>();
			var studentIdNumber = jObject.GetValue("studentIdNumber")?.Value<string>();

			// 制約条件を満たすデータのIDを指定
			jObject["groupId"] = db.Groups.Where(e => e.GroupCode == groupCode).FirstOrDefault()?.Id;
			jObject["studentId"] = db.Users.OfType<Student>().Where(e => e.StudentId == studentIdNumber).FirstOrDefault()?.Id;

			return base.CreateFromJObject(jObject);
		}

		// 並び替えの順序を設定
		protected override IQueryable<AttendGroup> OrderBy(IQueryable<AttendGroup> query)
		{
			return base.OrderBy(query).OrderBy(q => q.Group.GroupCode).ThenBy(q => q.Student.StudentId);
		}

		protected override IQueryable<AttendGroup> Where(IQueryable<AttendGroup> query, IDictionary<string, string> parameters)
		{
			// グループID指定
			if (parameters.TryGetValue("groupId", out var groupIdString) && int.TryParse(groupIdString, out var groupId))
			{
				query = query.Where(q => q.GroupId == groupId);
			}
			// 生徒ID指定
			if (parameters.TryGetValue("studentId", out var studentId))
			{
				query = query.Where(q => q.StudentId == studentId);
			}
			return base.Where(query, parameters);
		}

		protected override async Task<IManagerResult> ExecuteCreateAsync(AttendGroup entity)
		{
			var result = await base.ExecuteCreateAsync(entity);

			if (result is CreateSucceededManagerResult<AttendGroup> suceeded)
			{
				var db = new ApplicationDbContext();
				suceeded.Result.Student = db.Users.OfType<Student>().Where(e => e.Id == entity.StudentId).FirstOrDefault();
				suceeded.Result.Group = db.Groups.Where(e => e.Id == entity.GroupId).FirstOrDefault();
			}
			return result;
		}
	}
}