﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class LessonsManager :
		AbstractEntityManager<string, Lesson>
	{
		protected override Lesson CreateFromJObject(JObject jObject)
		{
			var db = new ApplicationDbContext();
			var startPeriodNumber = jObject.GetValue("startPeriod")?.Value<int>();
			var endPeriodNumber = jObject.GetValue("endPeriod")?.Value<int>();
			var teacherIdNumber = jObject.GetValue("teacherIdNumber")?.Value<int>();
			var classroomName = jObject.GetValue("classroomName")?.Value<string>();

			// 制約条件を満たすデータのIDを指定
			jObject["teacherId"] = db.Users.OfType<Teacher>().Where(e => e.TeacherId == teacherIdNumber).FirstOrDefault()?.Id;
			jObject["classroomId"] = db.Classrooms.Where(e => e.ClassroomName == classroomName).FirstOrDefault()?.Id;
			jObject["startPeriodId"] = db.Periods.Where(e => e.Number == startPeriodNumber).FirstOrDefault()?.Id;
			jObject["endPeriodId"] = db.Periods.Where(e => e.Number == endPeriodNumber).FirstOrDefault()?.Id;
			
			return base.CreateFromJObject(jObject);
		}
	}
}