﻿using ClosedXML.Excel;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models.Excel;

namespace TetsujinTimes.Net.Models.DataManagers.Abstracts
{
	public abstract class AbstractCommonManager<TKey, TValue> : AbstractRestManager<TKey, TValue>, IWritableToWorkbook where TValue : class
	{
		/// <summary>
		/// このオブジェクトに関連付けられているデータソースを返します。
		/// </summary>
		public abstract IQueryable<TValue> DataSource { get; }

		/// <summary>
		/// 初期化処理
		/// </summary>
		/// <param name="initialization">初期化処理を行う関数</param>
		/// <returns></returns>
		public virtual AbstractCommonManager<TKey, TValue> Initialize(Action<AbstractCommonManager<TKey, TValue>> initialization)
		{
			initialization?.Invoke(this);
			return this;
		}

		public override async Task<IManagerResult> QueryAsync(IDictionary<string, string> parameters)
		{
			// .xlsx形式でデータを取得
			if (parameters?.ContainsKey("xlsx") ?? false)
			{
				// .xlsファイルストリームを取得
				var workbook = WorkbookManager.CreateWorkbook();
				WriteToWorkbook(workbook);
				var stream = workbook.GetStream();

				// レスポンスを設定
				var filename = workbook.Worksheets.FirstOrDefault()?.Name ?? "無題";
				HttpResponseMessage response = new HttpResponseMessage
				{
					Content = new StreamContent(stream),
				};
				response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
				{
					FileName = $"{filename}.xlsx",
				};

				return ResponseMessage(response);
			}

			// それ以外

			IQueryable<TValue> query = ExecuteQuery(parameters);
			return QuerySuccess(query);
		}

		public override async Task<IManagerResult> GetAsync(TKey id)
		{
			if (!Exists(DataSource, id))
			{
				return NotFound();
			}
			var entity = Find(DataSource, id) as TValue;
			return GetSuccess(entity);
		}

		public override async Task<IManagerResult> CreateAsync(TValue entity)
		{
			try
			{
				//// ユーザを登録
				return await ExecuteCreateAsync(entity);
			}
			catch (DbUpdateException exception)
			{
				return DbExceptionHelper(exception);
			}
		}

		public override async Task<IManagerResult> UpdateAsync(TKey id, TValue entity)
		{
			if (!Exists(DataSource, id))
			{
				return NotFound();
			}

			if (!Verify(entity, id))
			{
				return Fail();
			}

			// 更新処理
			try
			{
				return await ExecuteUpdateAsync(id, entity);
			}
			catch (DbUpdateException exception)
			{
				return DbExceptionHelper(exception);
			}
		}

		public override async Task<IManagerResult> DeleteAsync(TKey id)
		{
			if (!Exists(DataSource, id))
			{
				return NotFound();
			}
			// 削除処理
			try
			{
				return await ExecuteDeleteAsync(id);
			}
			catch (DbUpdateException exception)
			{
				return DbExceptionHelper(exception);
			}
		}

		// Upload
		public virtual async Task<IManagerResult> UploadAsync(Stream stream)
		{
			// Excelファイルからインスタンスリストを取得
			IEnumerable<TValue> list;
			try
			{
				list = WorkbookManager.FromStream(stream).CreateCollection(CreateFromJObject);
			}
			catch (Exception ex)
			{
				return Fail(ex);
			}

			// 追加または更新処理
			var tasks = list.Select(async e => await CreateAsync(e));
			var results = new List<IManagerResult>();
			foreach (var task in tasks)
			{
				results.Add(await task);
			}

			var type = typeof(TValue);
			// 名前を取得
			var name = type.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;

			return QuerySuccess(new
			{
				name,
				results,
			});
		}

		// クエリを取得
		protected virtual IQueryable<TValue> ExecuteQuery(IDictionary<string, string> parameters = null)
		{
			IQueryable<TValue> query = DataSource;
			
			// データを並び替え
			query = OrderBy(query);

			// 一度に返却するデータの最大件数を指定(デフォルトでは50件)
			if (parameters != null)
			{
				// 渡されたクエリ条件を満たすデータのみを抽出
				query = Where(query, parameters);

				if (parameters.TryGetValue("size", out var sizeValue))
				{
					if (int.TryParse(sizeValue, out var requestSize))
					{
						// 1ページに表示するデータ数を指定
						int size = requestSize;

						// ページ数を指定
						if (parameters.TryGetValue("page", out var pageValue))
						{
							if (int.TryParse(pageValue, out var page) && page >= 1)
							{
								query = query.Skip((page - 1) * size);
							}
						}
						query = query.Take(size);
					}
				}
			}
			return query;
		}

		// ワークブックに書き込み
		public IXLWorkbook WriteToWorkbook(IXLWorkbook workbook)
		{
			var query = ExecuteQuery();
			workbook.AddWorksheetFromCollection(query);
			return workbook;
		}

		private IManagerResult DbExceptionHelper(DbUpdateException dbUpdateException)
		{
			if (dbUpdateException.InnerException is UpdateException updateException)
			{
				if (updateException.InnerException is SqlException sqlException)
				{
					return Fail($"{sqlException.Number}:{sqlException.Message}");
				}
			}
			return Fail(dbUpdateException);
		}


		/// <summary>
		/// 要素の新規作成を行う抽象メソッド
		/// </summary>
		/// <param name="entity"></param>
		protected abstract Task<IManagerResult> ExecuteCreateAsync(TValue entity);

		/// <summary>
		/// 要素の更新を行う抽象メソッド
		/// </summary>
		/// <param name="entity"></param>
		protected abstract Task<IManagerResult> ExecuteUpdateAsync(TKey id, TValue entity);

		/// <summary>
		/// 要素の削除を行う抽象メソッド
		/// </summary>
		/// <param name="entity"></param>
		protected abstract Task<IManagerResult> ExecuteDeleteAsync(TKey id);


		/// <summary>
		/// 条件を絞って検索
		/// </summary>
		/// <param name="query"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		protected virtual IQueryable<TValue> Where(IQueryable<TValue> query, IDictionary<string, string> parameters)
		{
			return query;
		}

		/// <summary>
		/// 並び替えのロジックを記述します。
		/// </summary>
		/// <param name="query"></param>
		/// <returns></returns>
		protected virtual IQueryable<TValue> OrderBy(IQueryable<TValue> query)
		{
			return query;
		}

		/// <summary>
		/// 指定した識別子で表される要素を返します。
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		protected virtual TValue Find(IQueryable<TValue> query, TKey id)
		{
			var predicate = GetIdentityExpression(id);
			return query.FirstOrDefault(predicate);
		}

		/// <summary>
		/// 指定した識別子で表される要素が存在しているかを判定します。
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		protected virtual bool Exists(IQueryable<TValue> query, TKey id)
		{
			var predicate = GetIdentityExpression(id);
			return query.Any(predicate);
		}

		/// <summary>
		/// 要素が指定した識別子で表されるかを検証します。
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		protected virtual bool Verify(TValue entity, TKey id)
		{
			var expression = GetIdentityExpression(id);
			return expression.Compile().Invoke(entity);
		}

		/// <summary>
		/// JObjectからインスタンスを生成します。
		/// </summary>
		/// <param name="jObject"></param>
		/// <returns></returns>
		protected virtual TValue CreateFromJObject(JObject jObject)
		{
			return jObject.ToObject<TValue>();
		}

		/// <summary>
		/// オブジェクトが指定した識別子で表されるかを評価する式ツリーを返します。
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		protected abstract Expression<Func<TValue, bool>> GetIdentityExpression(TKey id);
	}
}