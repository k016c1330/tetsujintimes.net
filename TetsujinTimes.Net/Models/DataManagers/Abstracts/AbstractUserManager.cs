﻿using ClosedXML.Excel;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading.Tasks;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.Excel;

namespace TetsujinTimes.Net.Models.DataManagers.Abstracts
{
	public class AbstractUserManager<TKey, TValue> : AbstractCommonManager<TKey, TValue>, IUploadableManager, IWritableToWorkbook where TValue : ApplicationUser
	{
		/// <summary>
		/// このオブジェクトに関連付けられているOwinContextを取得または設定します。
		/// </summary>
		public IOwinContext OwinContext { get; set; }

		/// <summary>
		/// この要求に関連付けられた現在のプリンシパルを取得または設定します。
		/// </summary>
		public IPrincipal User { get; set; }

		public ApplicationUserManager UserManager => OwinContext.GetUserManager<ApplicationUserManager>();
		public override IQueryable<TValue> DataSource => UserManager.Users.OfType<TValue>();

		protected override async Task<IManagerResult> ExecuteCreateAsync(TValue entity)
		{
			var createResult = await UserManager.CreateAsync(entity);
			if (!createResult.Succeeded)
			{
				throw new DbUpdateException(string.Join(", ", createResult.Errors));
			}
			//// パスワードを登録(未定義の場合はメールアドレスを設定)
			string password = entity.Email;
			var addPasswordResult = await UserManager.AddPasswordAsync(entity.Id, password);
			if (!addPasswordResult.Succeeded)
			{
				await UserManager.DeleteAsync(entity);
				throw new DbUpdateException(string.Join(", ", addPasswordResult.Errors));
			}
			return CreateSuccess(entity);
		}

		protected override async Task<IManagerResult> ExecuteUpdateAsync(TKey id, TValue entity)
		{
			var user = Find(DataSource,id) as TValue;
			Copy(user, entity);

			var updateResult = await UserManager.UpdateAsync(user);
			if (!updateResult.Succeeded)
			{
				throw new DbUpdateException(string.Join(", ", updateResult.Errors));
			}
			return UpdateSuccess(entity);
		}

		protected override async Task<IManagerResult> ExecuteDeleteAsync(TKey id)
		{
			var entity = Find(DataSource, id);
			var deleteResult = await UserManager.DeleteAsync(entity);
			if (!deleteResult.Succeeded)
			{
				throw new DbUpdateException(string.Join(", ", deleteResult.Errors));
			}
			return DeleteSuccess(entity);
		}
	
		/// <summary>
		/// 条件を絞って検索
		/// </summary>
		/// <param name="query"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		protected override IQueryable<TValue> Where(IQueryable<TValue> query, IDictionary<string, string> parameters)
		{
			query = base.Where(query, parameters);
			// 名前
			if (parameters.TryGetValue("name", out var name))
			{
				query = query.Where(t => t.Name.Contains(name));
			}
			// メールアドレス
			if (parameters.TryGetValue("email", out var email))
			{
				query = query.Where(t => t.Email.Contains(email));
			}
			return query;
		}

		protected override IQueryable<TValue> OrderBy(IQueryable<TValue> query)
		{
			return base.OrderBy(query).OrderBy(q => q.Id);
		}

		/// <summary>
		/// 指定した要素をコピーします。
		/// </summary>
		/// <param name="destination"></param>
		/// <param name="source"></param>
		protected virtual void Copy(TValue destination, TValue source)
		{
			destination.Id = source.Id;
			destination.Name = source.Name;
			destination.Email = source.Email;
		}
		
		/// <summary>
		/// オブジェクトが指定したユニークキーのペアで表されるかを評価する式ツリーを返します。
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		protected override Expression<Func<TValue, bool>> GetIdentityExpression(TKey id)
		{
			string idString = id as string;
			// 式ツリー形式で渡さないとLinq to EntitiesでSQL文に変換できない
			return e => e.Id == idString;
		}
	}
}