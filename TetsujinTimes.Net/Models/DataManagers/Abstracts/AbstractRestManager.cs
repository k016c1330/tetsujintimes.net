﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace TetsujinTimes.Net.Models.DataManagers.Abstracts
{
	public abstract class AbstractRestManager<TKey, TValue> : IRestManager<TKey, TValue>
	{
		public abstract Task<IManagerResult> CreateAsync(TValue entity);
		public abstract Task<IManagerResult> DeleteAsync(TKey id);
		public abstract Task<IManagerResult> GetAsync(TKey id);
		public abstract Task<IManagerResult> QueryAsync(IDictionary<string, string> parameters);
		public abstract Task<IManagerResult> UpdateAsync(TKey id, TValue entity);

		protected QuerySucceededManagerResult<T> QuerySuccess<T>(T content)
		{
			return new QuerySucceededManagerResult<T>(content);
		}

		protected QuerySucceededManagerResult<T> QuerySuccess<T>(T content, string contentType)
		{
			return new QuerySucceededManagerResult<T>(content)
			{
				ContentType = contentType,
			};
		}

		protected HttpResponseMessageResult ResponseMessage(HttpResponseMessage response)
		{
			return new HttpResponseMessageResult(response);
		}

		protected GetSucceededManagerResult<T> GetSuccess<T>(T content)
		{
			return new GetSucceededManagerResult<T>(content);
		}

		protected CreateSucceededManagerResult<T> CreateSuccess<T>(T content)
		{
			return new CreateSucceededManagerResult<T>(content);
		}

		protected UpdateSucceededManagerResult<T> UpdateSuccess<T>(T content)
		{
			return new UpdateSucceededManagerResult<T>(content);
		}

		protected DeleteSucceededManagerResult<T> DeleteSuccess<T>(T content)
		{
			return new DeleteSucceededManagerResult<T>(content);
		}
		
		protected FailedManagerResult Fail(string message = null)
		{
			return new FailedManagerResult(message);
		}

		protected FailedManagerResult Fail(Exception exception)
		{
			return new FailedManagerResult(exception);
		}

		protected NotFoundMangerResult NotFound(string message = null)
		{
			return new NotFoundMangerResult(message);
		}

	}
}