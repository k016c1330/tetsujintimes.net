﻿using ClosedXML.Excel;
using Microsoft.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading.Tasks;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.Excel;

namespace TetsujinTimes.Net.Models.DataManagers.Abstracts
{
	public abstract class AbstractEntityManager<TKey, TValue> : AbstractCommonManager<TKey, TValue>, IUploadableManager, IWritableToWorkbook where TValue : AbstractEntity
	{
		/// <summary>
		/// このオブジェクトに関連付けられているOwinContextを取得または設定します。
		/// </summary>
		public IOwinContext OwinContext { get; set; }

		/// <summary>
		/// この要求に関連付けられた現在のプリンシパルを取得または設定します。
		/// </summary>
		public IPrincipal User { get; set; }

		/// <summary>
		/// このオブジェクトに関連付けられているApplicationDbContextを取得します。
		/// </summary>
		protected ApplicationDbContext DbContext { get; set; } = new ApplicationDbContext(); //=> OwinContext?.Get<ApplicationDbContext>();

		public override IQueryable<TValue> DataSource => DbContext.Set<TValue>();
		
		protected override async Task<IManagerResult> ExecuteCreateAsync(TValue entity)
		{
			var db = new ApplicationDbContext();
			var source = db.Set<TValue>();
			
			source.Add(entity);
			await db.SaveChangesAsync();
			
			return CreateSuccess(entity);
		}

		protected override async Task<IManagerResult> ExecuteUpdateAsync(TKey id, TValue entity)
		{
			var db = new ApplicationDbContext();
			db.Entry(entity).State = EntityState.Modified;

			await db.SaveChangesAsync();
			
			return UpdateSuccess(entity);
		}

		protected override async Task<IManagerResult> ExecuteDeleteAsync(TKey id)
		{
			var db = new ApplicationDbContext();
			var source = db.Set<TValue>();
			var entity = Find(source, id) as TValue;
			entity = source.Remove(entity);
			
			await db.SaveChangesAsync();
			
			return DeleteSuccess(entity);
		}

		protected override IQueryable<TValue> OrderBy(IQueryable<TValue> query)
		{
			return base.OrderBy(query).OrderBy(q => q.Id);
		}

		/// <summary>
		/// オブジェクトが指定したユニークキーのペアで表されるかを評価する式ツリーを返します。
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		protected override Expression<Func<TValue, bool>> GetIdentityExpression(TKey id)
		{
			string idString = id as string;
			if (!int.TryParse(idString, out int idNumber)) return _ => false;

			// 式ツリー形式で渡さないとLinq to EntitiesでSQL文に変換できない
			return e => e.Id == idNumber;
		}
	}
}