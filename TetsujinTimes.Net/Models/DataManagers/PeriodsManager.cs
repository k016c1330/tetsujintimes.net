﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class PeriodsManager :
		AbstractEntityManager<string, Period>
	{
		protected override Expression<Func<Period, bool>> GetIdentityExpression(string id)
		{
			if (!int.TryParse(id, out int idNumber)) return _ => false;

			// 式ツリー形式で渡さないとLinq to EntitiesでSQL文に変換できない
			return e => e.Number == idNumber;
		}

		protected override IQueryable<Period> OrderBy(IQueryable<Period> query)
		{
			return query.OrderBy(e => e.Number);
		}
	}
}