﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class TeachersManager : AbstractUserManager<int, Teacher>
	{
		protected override IQueryable<Teacher> OrderBy(IQueryable<Teacher> query)
		{
			return query.OrderBy(e => e.TeacherId);
		}

		protected override void Copy(Teacher distination, Teacher source)
		{
			base.Copy(distination, source);
		}

		protected override Expression<Func<Teacher, bool>> GetIdentityExpression(int id)
		{
			return e => e.TeacherId == id;
		}

		protected override IQueryable<Teacher> Where(IQueryable<Teacher> query, IDictionary<string, string> parameters)
		{
			// 検索条件でデータをフィルタリング
			if (parameters.TryGetValue("teacherId", out var teacherIdString))
			{
				if (int.TryParse(teacherIdString, out var teacherId))
				{
					query = query.Where(t => t.TeacherId == teacherId);
				}
			}

			return base.Where(query, parameters);
		}
	}
}