﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class MemosManager :
		AbstractEntityManager<string, Memo>
	{
		protected override Memo CreateFromJObject(JObject jObject)
		{
			var db = new ApplicationDbContext();
			var lessonCode = jObject.GetValue("lessonCode")?.Value<string>();
			var studentId = jObject.GetValue("studentIdNumber")?.Value<string>();

			jObject["lessonId"] = db.Lessons.Where(e => e.LessonCode == lessonCode).FirstOrDefault()?.Id;
			jObject["studentId"] = db.Users.OfType<Student>().Where(e => e.StudentId == studentId).FirstOrDefault()?.Id;

			return base.CreateFromJObject(jObject);
		}

		protected override async Task<IManagerResult> ExecuteCreateAsync(Memo entity)
		{
			var result = await base.ExecuteCreateAsync(entity);

			if (result is CreateSucceededManagerResult<Memo> suceeded)
			{
				var db = new ApplicationDbContext();
				suceeded.Result.Student = db.Users.OfType<Student>().Where(e => e.Id == entity.StudentId).FirstOrDefault();
				suceeded.Result.Lesson = db.Lessons.Where(e => e.Id == entity.LessonId).FirstOrDefault();
			}
			return result;
		}

		protected override async Task<IManagerResult> ExecuteUpdateAsync(string id, Memo entity)
		{
			var result = await base.ExecuteUpdateAsync(id, entity);

			if (result is UpdateSucceededManagerResult<Memo> suceeded)
			{
				var db = new ApplicationDbContext();
				suceeded.Result.Student = db.Users.OfType<Student>().Where(e => e.Id == entity.StudentId).FirstOrDefault();
				suceeded.Result.Lesson = db.Lessons.Where(e => e.Id == entity.LessonId).FirstOrDefault();
			}
			return result;
		}
	}
}