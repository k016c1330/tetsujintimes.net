﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public interface IManagerResult
	{
	}

	/// <summary>
	/// 直接HttpResponseMessageを返す際のリザルト
	/// </summary>
	public class HttpResponseMessageResult : IManagerResult
	{
		public HttpResponseMessage Response { get; set; }
		public HttpResponseMessageResult(HttpResponseMessage response)
		{
			this.Response = response;
		}
	}

	/// <summary>
	/// 処理成功時に返されるManagerResult
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class SucceededManagerResult<T> : IManagerResult
	{
		public T Result { get; set; }
		public SucceededManagerResult(T result)
		{
			Result = result;
		}
	}

	public class QuerySucceededManagerResult<T> : SucceededManagerResult<T>
	{
		public string ContentType { get; set; }
		public QuerySucceededManagerResult(T result) : base(result)
		{
			ContentType = "application/json";
		}
	}

	public class GetSucceededManagerResult<T> : SucceededManagerResult<T>
	{
		public GetSucceededManagerResult(T result) : base(result)
		{
		}
	}

	public class CreateSucceededManagerResult<T> : SucceededManagerResult<T>
	{
		public CreateSucceededManagerResult(T result) : base(result)
		{
		}
	}
	public class UpdateSucceededManagerResult<T> : SucceededManagerResult<T>
	{
		public UpdateSucceededManagerResult(T result) : base(result)
		{
		}
	}

	public class DeleteSucceededManagerResult<T> : SucceededManagerResult<T>
	{
		public DeleteSucceededManagerResult(T result) : base(result)
		{
		}
	}



	/// <summary>
	/// 処理失敗時に返されるManagerResult
	/// </summary>
	public class FailedManagerResult : IManagerResult
	{
		public Exception Exception { get; set; }
		public string Message => Exception?.Message;

		public FailedManagerResult() { }
		public FailedManagerResult(string message)
		{
			Exception = new Exception(message);
		}
		public FailedManagerResult(Exception exception)
		{
			Exception = exception;
		}
	}

	public class NotFoundMangerResult : FailedManagerResult
	{
		public NotFoundMangerResult() { }
		public NotFoundMangerResult(string message) : base(message) { }
	}

}