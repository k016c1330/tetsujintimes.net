﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Newtonsoft.Json.Linq;
using TetsujinTimes.Net.Extensions;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class StudentsManager : AbstractUserManager<string, Student>
	{
		protected override void Copy(Student distination, Student source)
		{
			base.Copy(distination, source);

			//distination.StudentId = source.StudentId;
			distination.DepartmentId = source.DepartmentId;
			distination.Grade = source.Grade;
			distination.ClassNum = source.ClassNum;
		}

		protected override IQueryable<Student> OrderBy(IQueryable<Student> query)
		{
			return query.OrderBy(e => e.StudentId);
		}

		protected override Expression<Func<Student, bool>> GetIdentityExpression(string id)
		{
			return e => e.StudentId == id;
		}

		// 検索条件に一致する値のみを返す
		protected override IQueryable<Student> Where(IQueryable<Student> query, IDictionary<string, string> parameters)
		{
			// 学籍番号
			if (parameters.TryGetString("studentId", out var studentId))
			{
				query = query.Where(q => q.StudentId.Contains(studentId));
			}
			// 学科
			if (parameters.TryGetString("departmentName", out var departmentName))
			{
				// 学科コードまたは学科名に一致するものを抽出
				query = query.Where(q =>
					q.Department.DepartmentCode.Contains(departmentName) ||
					q.Department.DepartmentName.Contains(departmentName));
			}
			// 学年
			if (parameters.TryGetInteger("grade", out var grade))
			{
				query = query.Where(q => q.Grade == grade);
			}
			// 組
			if (parameters.TryGetInteger("classNum", out var classNum))
			{
				query = query.Where(q => q.ClassNum == classNum);
			}
			return base.Where(query, parameters);
		}

		protected override Student CreateFromJObject(JObject jObject)
		{
			var db = new ApplicationDbContext();
			var departmentCode = jObject.GetValue("departmentCode")?.Value<string>();
			
			jObject["departmentId"] = db.Departments.Where(e => e.DepartmentCode == departmentCode).FirstOrDefault()?.Id;

			return base.CreateFromJObject(jObject);
		}
	}
}