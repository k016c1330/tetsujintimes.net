﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class LessonGroupsManager :
		AbstractEntityManager<string, LessonGroup>
	{
		protected override LessonGroup CreateFromJObject(JObject jObject)
		{
			var db = new ApplicationDbContext();
			var groupCode = jObject.GetValue("groupCode")?.Value<string>();
			var lessonCode = jObject.GetValue("lessonCode")?.Value<string>();

			// 制約条件を満たすデータのIDを指定
			jObject["groupId"] = db.Groups.Where(e => e.GroupCode == groupCode).FirstOrDefault()?.Id;
			jObject["lessonId"] = db.Lessons.Where(e => e.LessonCode == lessonCode).FirstOrDefault()?.Id;

			return base.CreateFromJObject(jObject);
		}

		// 並び替えの順序を設定
		protected override IQueryable<LessonGroup> OrderBy(IQueryable<LessonGroup> query)
		{
			return base.OrderBy(query).OrderBy(q => q.Group.GroupCode).ThenBy(q => q.Lesson.LessonCode);
		}

		protected override IQueryable<LessonGroup> Where(IQueryable<LessonGroup> query, IDictionary<string, string> parameters)
		{
			// 授業ID
			if (parameters.TryGetValue("lessonId", out var lessonIdString) && int.TryParse(lessonIdString, out var lessonId))
			{
				query = query.Where(q => q.LessonId == lessonId);
			}
			return base.Where(query, parameters);
		}

		protected override async Task<IManagerResult> ExecuteCreateAsync(LessonGroup entity)
		{
			var result = await base.ExecuteCreateAsync(entity);

			if (result is CreateSucceededManagerResult<LessonGroup> suceeded)
			{
				var db = new ApplicationDbContext();
				suceeded.Result.Lesson = db.Lessons.Where(e => e.Id == entity.LessonId).FirstOrDefault();
				suceeded.Result.Group = db.Groups.Where(e => e.Id == entity.GroupId).FirstOrDefault();
			}
			return result;
		}
	}
}