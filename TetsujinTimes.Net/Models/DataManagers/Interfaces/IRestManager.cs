﻿using ClosedXML.Excel;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public interface IRestManager<TKey, TValue>
	{
		Task<IManagerResult> QueryAsync(IDictionary<string, string> parameters = null);
		Task<IManagerResult> GetAsync(TKey id);
		Task<IManagerResult> CreateAsync(TValue entity);
		Task<IManagerResult> UpdateAsync(TKey id, TValue entity);
		Task<IManagerResult> DeleteAsync(TKey id);
	}

	public interface IUploadableManager
	{
		IOwinContext OwinContext { get; set; }
		IPrincipal User { get; set; }

		Task<IManagerResult> UploadAsync(Stream stream);
	}

	public interface IWritableToWorkbook
	{
		IXLWorkbook WriteToWorkbook(IXLWorkbook workbook);
	}

}