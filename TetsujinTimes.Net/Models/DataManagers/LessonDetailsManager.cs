﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class LessonDetailsManager :
		AbstractEntityManager<string, LessonDetail>
	{
	}
}