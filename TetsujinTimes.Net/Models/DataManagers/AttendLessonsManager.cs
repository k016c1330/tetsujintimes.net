﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using Newtonsoft.Json.Linq;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class AttendLessonsManager :
		AbstractEntityManager<string, AttendLesson>
	{
		protected override AttendLesson CreateFromJObject(JObject jObject)
		{
			var db = new ApplicationDbContext();
			var lessonCode = jObject.GetValue("lessonCode")?.Value<string>();
			var studentId = jObject.GetValue("studentIdNumber")?.Value<string>();

			jObject["lessonId"] = db.Lessons.Where(e => e.LessonCode == lessonCode).FirstOrDefault()?.Id;
			jObject["studentId"] = db.Users.OfType<Student>().Where(e => e.StudentId == studentId).FirstOrDefault()?.Id;

			return base.CreateFromJObject(jObject);
		}
	}
}