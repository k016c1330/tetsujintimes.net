﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class ClassroomsManager :
		AbstractEntityManager<string, Classroom>
	{
		protected override Expression<Func<Classroom, bool>> GetIdentityExpression(string id)
		{
			return e => e.ClassroomName == id;
		}

		protected override IQueryable<Classroom> OrderBy(IQueryable<Classroom> query)
		{
			return query.OrderBy(e => e.ClassroomName);
		}
	}
}