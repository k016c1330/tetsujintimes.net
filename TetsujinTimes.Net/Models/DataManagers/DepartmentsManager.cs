﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using System.Linq.Expressions;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class DepartmentsManager :
		AbstractEntityManager<string, Department>
	{
		protected override Expression<Func<Department, bool>> GetIdentityExpression(string id)
		{
			return e => e.DepartmentCode == id;
		}

		protected override IQueryable<Department> OrderBy(IQueryable<Department> query)
		{
			return query.OrderBy(e => e.DepartmentCode);
		}
	}
}