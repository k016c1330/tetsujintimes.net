﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;
using TetsujinTimes.Net.Models.DataModels.Abstracts;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class TimeTablesManager :
		AbstractEntityManager<string, TimeTable>
	{
		protected override TimeTable CreateFromJObject(JObject jObject)
		{
			var db = new ApplicationDbContext();
			var lessonCode = jObject.GetValue("lessonCode")?.Value<string>();
			var departmentCode = jObject.GetValue("departmentCode")?.Value<string>();

			jObject["lessonId"] = db.Lessons.Where(e => e.LessonCode == lessonCode).FirstOrDefault()?.Id;
			jObject["departmentId"] = db.Departments.Where(e => e.DepartmentCode == departmentCode).FirstOrDefault()?.Id;
			
			return base.CreateFromJObject(jObject);
		}
	}
}