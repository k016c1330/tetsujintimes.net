﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TetsujinTimes.Net.Models.DataManagers.Abstracts;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Models.DataManagers
{
	public class UsersManager : AbstractUserManager<string, ApplicationUser>
	{
		// 読み取り専用：編集不可（追加、上書き、削除）

		public override Task<IManagerResult> CreateAsync(ApplicationUser entity)
		{
			throw new NotImplementedException();
		}

		public override Task<IManagerResult> DeleteAsync(string id)
		{
			throw new NotImplementedException();
		}

		public override Task<IManagerResult> UpdateAsync(string id, ApplicationUser entity)
		{
			throw new NotImplementedException();
		}
	}
}