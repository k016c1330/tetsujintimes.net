﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace TetsujinTimes.Net.Models
{
	public class ApplicationRole : IdentityRole
	{
		/// <summary>
		/// ユーザのアクセスレベル
		/// </summary>
		public int Level { get; set; }


		/// <summary>
		/// 管理者ロール
		/// </summary>
		public static readonly ApplicationRole Administrator = new ApplicationRole
		{
			Name = nameof(Administrator),
			Level = 7,
		};

		/// <summary>
		/// 教師ロール
		/// </summary>
		public static readonly ApplicationRole Teacher = new ApplicationRole
		{
			Name = nameof(Teacher),
			Level = 4,
		};

		/// <summary>
		/// 生徒ロール
		/// </summary>
		public static readonly ApplicationRole Student = new ApplicationRole
		{
			Name = nameof(Student),
			Level = 1,
		};

		/// <summary>
		/// デフォルトで登録されるロール一覧
		/// </summary>
		public static readonly IEnumerable<ApplicationRole> DefaultRoles =
			new ReadOnlyCollection<ApplicationRole>(new ApplicationRole[]
		{
			Administrator,
			Student,
			Teacher
		});
	}

	public class ApplicationRoleManager : RoleManager<ApplicationRole, string>
	{
		public ApplicationRoleManager(IRoleStore<ApplicationRole, string> store) : base(store)
		{
		}
		public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
		{
			var dbContext = context.Get<ApplicationDbContext>();
			var roleStore = new RoleStore<ApplicationRole>(dbContext);
			var manager = new ApplicationRoleManager(roleStore);

			if (!manager.Roles.Any())
			{
				// 初回にロールを登録する
				foreach (var role in ApplicationRole.DefaultRoles)
				{
					manager.Create(role);
				}
			}

			return manager;
		}
	}
}