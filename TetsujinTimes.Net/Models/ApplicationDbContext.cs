﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TetsujinTimes.Net.Models.DataModels;

namespace TetsujinTimes.Net.Models
{
	/// <summary>
	/// アプリケーションのデータベースコンテキスト
	/// </summary>
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext()
			: base("DefaultConnection", throwIfV1Schema: false)
		{
		}

		public static ApplicationDbContext Create()
		{
			return new ApplicationDbContext();
		}

		public virtual IDbSet<Department> Departments { get; set; }
		public virtual IDbSet<Lesson> Lessons { get; set; }
		public virtual IDbSet<AttendLesson> AttendLessons { get; set; }
		public virtual IDbSet<Classroom> Classrooms { get; set; }
		public virtual IDbSet<TimeTable> TimeTables { get; set; }
		public virtual IDbSet<Memo> Memos { get; set; }
		public virtual IDbSet<Period> Periods { get; set; }

		public virtual IDbSet<Group> Groups { get; set; }
		public virtual IDbSet<AttendGroup> AttendGroups { get; set; }
		public virtual IDbSet<LessonGroup> LessonGroups { get; set; }

		// 追加されたDB Set
		public override IDbSet<ApplicationUser> Users { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			//modelBuilder.Entity<Teacher>()
			//	.HasRequired(s => s.User)
			//	.WithMany()
			//	.WillCascadeOnDelete(false);

			//modelBuilder.Entity<Student>()
			//	.HasRequired(s => s.User)
			//	.WithMany()
			//	.WillCascadeOnDelete(false);

			modelBuilder.Entity<Lesson>()
				.HasRequired(s => s.Teacher)
				.WithMany()
				.WillCascadeOnDelete(false);
			
			modelBuilder.Entity<Lesson>()
				.HasRequired(s => s.StartPeriod)
				.WithMany()
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Lesson>()
				.HasRequired(s => s.EndPeriod)
				.WithMany()
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<Attendance>()
				.HasRequired(s => s.LessonDetail)
				.WithMany()
				.WillCascadeOnDelete(false);
		}

		public System.Data.Entity.DbSet<TetsujinTimes.Net.Models.DataModels.LessonDetail> LessonDetails { get; set; }

		public System.Data.Entity.DbSet<TetsujinTimes.Net.Models.DataModels.Attendance> Attendances { get; set; }
	}

	public static class ApplicationDbContextHelper
	{
	}

	/// <summary>
	/// ApplicationDbContextのイニシャライザ
	/// </summary>
	public class ApplicationDbContextInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
	{
		protected override void Seed(ApplicationDbContext context)
		{
			base.Seed(context);

			// 初期データの登録
			if (!context.Departments.Any())
			{
				new List<Department>
				{
					new Department { DepartmentCode = "CD", DepartmentName = "情報処理科" },
					new Department { DepartmentCode = "IS", DepartmentName = "ITスペシャリスト科" },
				}
				.ForEach(e => context.Departments.Add(e));
				context.SaveChanges();
			}
			if (!context.Classrooms.Any())
			{
				new List<Classroom>
				{
					new Classroom{ ClassroomName="30711" },
					new Classroom{ ClassroomName="30712" },
					new Classroom{ ClassroomName="30713" },
				}
				.ForEach(e => context.Classrooms.Add(e));
				context.SaveChanges();
			}
			if (!context.Periods.Any())
			{
				new List<Period>
				{
					new Period { Number = 1, StartTime = TimeSpan.Parse("09:00"), EndTime = TimeSpan.Parse("09:45") },
					new Period { Number = 2, StartTime = TimeSpan.Parse("09:45"), EndTime = TimeSpan.Parse("10:30") },
					new Period { Number = 3, StartTime = TimeSpan.Parse("10:40"), EndTime = TimeSpan.Parse("11:25") },
					new Period { Number = 4, StartTime = TimeSpan.Parse("11:25"), EndTime = TimeSpan.Parse("12:10") },
					new Period { Number = 5, StartTime = TimeSpan.Parse("13:00"), EndTime = TimeSpan.Parse("13:45") },
					new Period { Number = 6, StartTime = TimeSpan.Parse("13:45"), EndTime = TimeSpan.Parse("14:30") },
					new Period { Number = 7, StartTime = TimeSpan.Parse("14:40"), EndTime = TimeSpan.Parse("15:25") },
					new Period { Number = 8, StartTime = TimeSpan.Parse("15:25"), EndTime = TimeSpan.Parse("16:10") },
				}
				.ForEach(e => context.Periods.Add(e));
				context.SaveChanges();
			}

			if (!context.Users.OfType<Student>().Any())
			{
				new List<Student>
				{
					new Student
					{
						StudentId = "K016C0000",
						Name = "情報太郎",
						Email = "taro@it-neec.jp",
						DepartmentId = context.Departments.First().Id,
						Grade = 1,
						ClassNum = 1,
					},
					new Student
					{
						StudentId = "K016C0001",
						Name = "情報次郎",
						Email = "jiro@it-neec.jp",
						DepartmentId = context.Departments.First().Id,
						Grade = 1,
						ClassNum = 3,
					},
					new Student
					{
						StudentId = "K016C0002",
						Name = "情報花子",
						Email = "hanako@it-neec.jp",
						DepartmentId = context.Departments.First().Id,
						Grade = 1,
						ClassNum = 3,
					},
				}
				.ForEach(e => context.Users.Add(e));
				context.SaveChanges();
			}

			if (!context.Users.OfType<Teacher>().Any())
			{
				new List<Teacher>
				{
					new Teacher
					{
						TeacherId = 1,
						Name = "教師一郎",
						Email = "Teacher01@it-neec.jp",
					},
					new Teacher
					{
						TeacherId = 2,
						Name = "教師次郎",
						Email = "Teacher02@it-neec.jp",
					},
				}
				.ForEach(e => context.Users.Add(e));
				context.SaveChanges();
			}
		}
	}
}
