﻿using Microsoft.Azure.NotificationHubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TetsujinTimes.Net.Models
{
	public class Notifications
	{
		// 通知ハブへの接続文字列
		public static readonly string DefaultFullSharedAccessSignature = "Endpoint=sb://tetsujintimes.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=QkIgvaihSSncY1B6aRwoqVzQCeVTV1qdUA+NukYH1kk=";
		// 通知ハブ名
		public static readonly string HubName = "TetsujinTimesNotification";

		public static Notifications Instance = new Notifications();

		public NotificationHubClient Hub { get; set; }

		private Notifications()
		{
			Hub = NotificationHubClient
				.CreateClientFromConnectionString(DefaultFullSharedAccessSignature, HubName);
		}
	}
}