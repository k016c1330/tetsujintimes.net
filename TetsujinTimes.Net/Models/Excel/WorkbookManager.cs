﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.InteropServices;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.IO;
using ClosedXML.Excel;
using Newtonsoft.Json;
using TetsujinTimes.Net.Models.DataModels.Attributes;

namespace TetsujinTimes.Net.Models.Excel
{
	public static class WorkbookManager
	{
		public static IXLWorkbook CreateWorkbook()
		{
			var workbook = new XLWorkbook();
			return workbook;
		}

		public static IXLWorkbook FromStream(Stream stream)
		{
			return new XLWorkbook(stream);
		}

		public static IXLWorkbook AddWorksheetFromCollection<T>(this IXLWorkbook workbook, IEnumerable<T> collection)
		{
			// コレクションの型名を取得
			var info = typeof(T);
			var title = info.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName ?? info.Name;

			var worksheet = workbook.Worksheets.Add(title);

			// フォントの設定
			worksheet.Style.Font.SetFontName("MS P Gothic");
			worksheet.Style.Font.SetFontSize(11.0);

			// すべてのデータプロパティを取り出す
			var properties =
				typeof(T).GetProperties()
					.Where(p => p.GetCustomAttribute<WorksheetColumnAttribute>() != null)
					.Where(p => p.GetCustomAttribute<DisplayNameAttribute>() != null)
					.Where(p => p.GetCustomAttribute<JsonPropertyAttribute>() != null)
					.Select(p => new
					{
						p.GetCustomAttribute<WorksheetColumnAttribute>()?.Index,
						p.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName,
						p.GetCustomAttribute<JsonPropertyAttribute>()?.PropertyName,
					})
					.OrderBy(p => p.Index);

			// ワークシートに書き込み
			int row, column;
			row = 1;
			column = 1;
			foreach (var property in properties)
			{
				var cell = worksheet.Cell(row, column);
				cell.Value = property.DisplayName;
				cell.RichText.Bold = true;
				column += 1;
			}
			row += 1;
			foreach (var item in collection)
			{
				JToken token = JToken.FromObject(item);

				column = 1;
				foreach (var property in properties)
				{
					var value = token[property.PropertyName];

					var cell = worksheet.Cell(row, column);
					cell.Value = value.ToString();
					column += 1;
				}
				row += 1;
			}

			// 列のサイズの自動調整
			foreach (var worksheetColumn in worksheet.Columns())
			{
				worksheetColumn.AdjustToContents();
			}

			return workbook;
		}
		
		public static Stream GetStream(this IXLWorkbook workbook)
		{
			// 新規メモリストリームを作成。
			var stream = new MemoryStream();
			// ストリームにExcelファイルを保存
			workbook.SaveAs(stream);
			// ストリームの位置を最初に戻す
			stream.Seek(0, SeekOrigin.Begin);

			// ここで作成したストリームは必ず解放すること！
			return stream;
		}

		/// <summary>
		///  エクセルシートから指定した型のコレクションを読み取ります。
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="workbook"></param>
		/// <returns></returns>
		public static IEnumerable<T> CreateCollection<T>(this IXLWorkbook workbook, Func<JObject, T> converter = null)
		{
			// 型情報を取得
			var info = typeof(T);
			var displayName = info.GetCustomAttribute<DisplayNameAttribute>();

			// プロパティ情報を取得
			var properties = info.GetProperties()
				.Where(p => p.GetCustomAttribute<WorksheetColumnAttribute>() != null)
				.Select(p => new
				{
					Property = p,
					DisplayName = p.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName,
					PropertyName = p.GetCustomAttribute<JsonPropertyAttribute>()?.PropertyName,
				})
				.Where(p => p.DisplayName != null && p.PropertyName != null)
				.ToDictionary(k => k.DisplayName, k => k.PropertyName);

			// ワークシートを取得
			var sheet = workbook?.Worksheets?.FirstOrDefault(s => s.Name == displayName.DisplayName);

			if (sheet == null) throw new FileFormatException("シートを読み込めませんでした。");
			if (sheet?.RowCount() == 0) throw new FileFormatException("行数が0です。");


			// 列数を取得
			var cells = sheet.Rows().FirstOrDefault()?.Cells();
			if (cells == null) throw new FileFormatException("列数が0です。");

			// 列名を取得
			var keys = new Dictionary<int, string>();
			foreach (var cell in cells)
			{
				var rowName = cell.GetValue<string>();
				keys[cell.Address.ColumnNumber] = properties[rowName];
			}

			// 各行の要素を取得
			var list = new List<T>();
			foreach (var row in sheet.Rows().Skip(1))
			{
				var jobj = new JObject();
				foreach (var key in keys)
				{
					var index = key.Key;
					var cell = row.Cell(index);
					var pName = key.Value;
					jobj[pName] = cell.GetValue<string>();
				}

				try
				{
					// 型変換して追加
					T item;
					if (converter != null)
					{
						item = converter(jobj);
					}
					else
					{
						item = jobj.ToObject<T>();
					}
					list.Add(item);
				}
				catch (Exception ex)
				{
					continue;
				}
			}

			return list;
		}
	}
}