﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Web;

//namespace TetsujinTimes.Net.Models
//{
//	/// <summary>
//	/// 識別用の式木を登録するディクショナリクラス
//	/// </summary>
//	public static class IdentityKeyConfiguration
//	{
//		private static Dictionary<Type, Expression> store = new Dictionary<Type, Expression>();

//		public static void Register<T>(Expression<Func<T, object>> expression)
//		{
//			store.Add(typeof(T), expression);
//		}

//		/// <summary>
//		/// 指定された型の識別キーを取得
//		/// </summary>
//		/// <typeparam name="T"></typeparam>
//		/// <returns></returns>
//		public static Expression<Func<T, object>> GetExpression<T>()
//		{
//			Type key = typeof(T);
//			if (!store.ContainsKey(key))
//			{
//				throw new Exception("実行時エラー：指定された型がストアに存在しません。");
//			}
//			var expression = store[key] as Expression<Func<T, object>>;
//			if (expression == null)
//			{
//				throw new Exception("実行時エラー：指定された型に関連付けられた式木のジェネリック型引数が異なります。");
//			}
//			return expression;
//		}
//	}

//}